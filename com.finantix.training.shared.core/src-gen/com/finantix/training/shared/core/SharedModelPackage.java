package com.finantix.training.shared.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.texo.model.ModelFactory;
import org.eclipse.emf.texo.model.ModelPackage;
import org.eclipse.emf.texo.model.ModelResolver;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.model.Models;
import com.thedigitalstack.model.internal.metadata.DaoRegistry;
import com.thedigitalstack.model.metadata.IDaoRegistry;
import com.thedigitalstack.model.metadata.TenantModelPackage;

/**
 * The <b>Package</b> for the model '<em><b>shared</b></em>'. It contains initialization code and access to the Factory to instantiate types of this package.<br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */
public class SharedModelPackage extends TenantModelPackage {

    /**
     * The package namespace URI.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    public static final String NS_URI = "com.finantix.training.shared.core.model";

    /**
     * The package name.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final String NAME = "shared";

    /**
     * The classifier id for the '{@link RefData <em>RefData</em>}' class.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int REFDATA_CLASSIFIER_ID = 0;

    /**
     * The feature id for the '{@link RefData <em>displayValue</em>}' attribute.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int REFDATA_DISPLAYVALUE_FEATURE_ID = 0;

    /**
     * The feature id for the '{@link RefData <em>displayOrder</em>}' attribute.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int REFDATA_DISPLAYORDER_FEATURE_ID = 1;

    /**
     * The feature id for the '{@link RefData <em>startValidationDate</em>}' attribute.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int REFDATA_STARTVALIDATIONDATE_FEATURE_ID = 2;

    /**
     * The feature id for the '{@link RefData <em>endValidationDate</em>}' attribute.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int REFDATA_ENDVALIDATIONDATE_FEATURE_ID = 3;

    /**
     * The feature id for the '{@link RefData <em>replacedBy</em>}' attribute.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public static final int REFDATA_REPLACEDBY_FEATURE_ID = 4;

    /**
     * The {@link ModelFactory} for this package.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private SharedModelFactory factory;

    /**
     * The {@com.thedigitalstack.model.metadata.IDaoRegistry} for this package.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    private static final IDaoRegistry daoRegistry = new DaoRegistry(NS_URI); // NOSONAR

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    static {

    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    public SharedModelPackage() {// NOSONAR
    }

    /**
     * Returns the {@link SharedModelFactory} of this ModelPackage.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the {@link SharedModelFactory} instance.
     * @generated
     */
    public SharedModelFactory getFactory() {
        return this.factory;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    public IDaoRegistry getDaoRegistry() {
        return daoRegistry;
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @SuppressWarnings("unchecked")
    @Override
    @NonNull
    protected Class<TenantModelPackage>[] getLinkedModelPackageClasses() {
        List<Class<? extends TenantModelPackage>> list = new ArrayList<Class<? extends TenantModelPackage>>();

        Class<TenantModelPackage>[] result = new Class[list.size()];
        list.toArray(result);
        return result;
    }

    /**
     * Initializes this {@link ModelPackage}.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    protected void doRegister(ResourceSet resourceSet, ModelResolver resolver) { // NOSONAR

        if (!registerModelPackageFromFile(resolver, resourceSet)) {
            return;
        }
        super.doRegister(resourceSet, resolver);
        factory = new SharedModelFactory(this);

        // read the model from the ecore file, the EPackage is registered in the EPackage.Registry
        // see the ModelResolver getEPackageRegistry method
        resolver.registerModelPackage(this);

        // register the relation between a Class and its EClassifier

        resolver.registerClassModelMapping(RefData.class, this.getRefDataEClass(), this);

    }

    /**
     * Returns the {@link ModelFactory} of this ModelPackage.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the {@link SharedModelFactory} instance.
     * @generated
     */
    @Override
    public SharedModelFactory getModelFactory() {
        return factory;
    }

    /**
     * Returns the nsUri of the {@link EPackage} managed by this Package instance.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the nsUri of the EPackage
     * @generated
     */
    @Override
    public String getNsURI() {
        return NS_URI;
    }

    /**
     * Returns the name of the ecore file containing the ecore model of the {@link EPackage} managed here.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the name of the ecore file
     * @generated
     */
    @Override
    public String getEcoreFileName() {
        return "shared.ecore";
    }

    /**
     * Returns the {@link EClass} '<em><b>RefData</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EClass} '<em><b>RefData</b></em>'
     * @generated
     */
    public EClass getRefDataEClass() {
        return (EClass) getEPackage().getEClassifiers().get(REFDATA_CLASSIFIER_ID);
    }

    /**
     * Returns the {@link EStructuralFeature} '<em><b>RefData.displayValue</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EStructuralFeature}: '<em><b>RefData.displayValue</b></em>'.
     * @generated
     */

    public EAttribute getRefData_DisplayValue() {
        return (EAttribute) getRefDataEClass().getEAllStructuralFeatures().get(REFDATA_DISPLAYVALUE_FEATURE_ID);
    }

    /**
     * Returns the {@link EStructuralFeature} '<em><b>RefData.displayOrder</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EStructuralFeature}: '<em><b>RefData.displayOrder</b></em>'.
     * @generated
     */

    public EAttribute getRefData_DisplayOrder() {
        return (EAttribute) getRefDataEClass().getEAllStructuralFeatures().get(REFDATA_DISPLAYORDER_FEATURE_ID);
    }

    /**
     * Returns the {@link EStructuralFeature} '<em><b>RefData.startValidationDate</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EStructuralFeature}: '<em><b>RefData.startValidationDate</b></em>'.
     * @generated
     */

    public EAttribute getRefData_StartValidationDate() {
        return (EAttribute) getRefDataEClass().getEAllStructuralFeatures().get(REFDATA_STARTVALIDATIONDATE_FEATURE_ID);
    }

    /**
     * Returns the {@link EStructuralFeature} '<em><b>RefData.endValidationDate</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EStructuralFeature}: '<em><b>RefData.endValidationDate</b></em>'.
     * @generated
     */

    public EAttribute getRefData_EndValidationDate() {
        return (EAttribute) getRefDataEClass().getEAllStructuralFeatures().get(REFDATA_ENDVALIDATIONDATE_FEATURE_ID);
    }

    /**
     * Returns the {@link EStructuralFeature} '<em><b>RefData.replacedBy</b></em>'.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return an instance of the {@link EStructuralFeature}: '<em><b>RefData.replacedBy</b></em>'.
     * @generated
     */

    public EAttribute getRefData_ReplacedBy() {
        return (EAttribute) getRefDataEClass().getEAllStructuralFeatures().get(REFDATA_REPLACEDBY_FEATURE_ID);
    }

    /**
     * Returns the class implementing a specific {@link EClass}.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param eClassifier
     *            the {@link EClassifier}
     * @return the class implementing a specific {@link EClass}.
     * @generated
     */
    @Override
    public Class<?> getEClassifierClass(EClassifier eClassifier) {
        switch (eClassifier.getClassifierID()) {

            case REFDATA_CLASSIFIER_ID:
                return RefData.class;

        }
        throw new IllegalArgumentException("The EClassifier '" + eClassifier + "' is not defined in this EPackage");
    }

    /**
     * Returns the class container of the list. <br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the class container of the list {@link EClass}.
     * @generated
     */
    public Collection<Class<?>> getContainers() {
        Collection<Class<?>> containers = new ArrayList<Class<?>>();

        return containers;
    }

    /**
     * Creates a container for a specific {@link EClass}.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param eClass
     *            creates a Object instance for this EClass
     * @return an object that representing the container of the EClass instances
     * @generated
     */
    public Class<?> getContainer(EClass eClass) {
        switch (eClass.getClassifierID()) {
        }
        throw new IllegalArgumentException("The EClass '" + eClass.getName() + "' is not a valid EClass for this EPackage");
    }

    /**
     * Returns the instance of the model Package.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the instance of the model Package
     * @generated
     */
    public static SharedModelPackage getInstance() {
        SharedModelPackage p = (SharedModelPackage) Models.getMetadataManager().getModelPackage(NS_URI);
        assert p != null;
        return p;
    }

}
