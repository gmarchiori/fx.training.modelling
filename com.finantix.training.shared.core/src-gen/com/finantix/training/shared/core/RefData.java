package com.finantix.training.shared.core;

import java.util.Date;

/**
 * A representation of the model object '<em><b>RefData</b></em>'.<br>
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

public interface RefData

{

    /**
     * Returns the value of '<em><b>displayValue</b></em>' feature.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of '<em><b>displayValue</b></em>' feature
     * @generated
     */
    public String getDisplayValue();

    /**
     * Sets the '{@link RefData#getDisplayValue() <em>displayValue</em>}' feature.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param newDisplayValue
     *            the new value of the '{@link RefData#getDisplayValue() <em>displayValue</em>}' feature.
     * @generated
     */
    public void setDisplayValue(String newDisplayValue);

    /**
     * Returns the value of '<em><b>displayOrder</b></em>' feature.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of '<em><b>displayOrder</b></em>' feature
     * @generated
     */
    public String getDisplayOrder();

    /**
     * Sets the '{@link RefData#getDisplayOrder() <em>displayOrder</em>}' feature.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param newDisplayOrder
     *            the new value of the '{@link RefData#getDisplayOrder() <em>displayOrder</em>}' feature.
     * @generated
     */
    public void setDisplayOrder(String newDisplayOrder);

    /**
     * Returns the value of '<em><b>startValidationDate</b></em>' feature.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of '<em><b>startValidationDate</b></em>' feature
     * @generated
     */
    public Date getStartValidationDate();

    /**
     * Sets the '{@link RefData#getStartValidationDate() <em>startValidationDate</em>}' feature.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param newStartValidationDate
     *            the new value of the '{@link RefData#getStartValidationDate() <em>startValidationDate</em>}' feature.
     * @generated
     */
    public void setStartValidationDate(Date newStartValidationDate);

    /**
     * Returns the value of '<em><b>endValidationDate</b></em>' feature.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of '<em><b>endValidationDate</b></em>' feature
     * @generated
     */
    public Date getEndValidationDate();

    /**
     * Sets the '{@link RefData#getEndValidationDate() <em>endValidationDate</em>}' feature.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param newEndValidationDate
     *            the new value of the '{@link RefData#getEndValidationDate() <em>endValidationDate</em>}' feature.
     * @generated
     */
    public void setEndValidationDate(Date newEndValidationDate);

    /**
     * Returns the value of '<em><b>replacedBy</b></em>' feature.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @return the value of '<em><b>replacedBy</b></em>' feature
     * @generated
     */
    public String getReplacedBy();

    /**
     * Sets the '{@link RefData#getReplacedBy() <em>replacedBy</em>}' feature.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param newReplacedBy
     *            the new value of the '{@link RefData#getReplacedBy() <em>replacedBy</em>}' feature.
     * @generated
     */
    public void setReplacedBy(String newReplacedBy);

}
