package com.finantix.training.shared.core;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.texo.model.ModelFactory;
import org.eclipse.emf.texo.model.ModelFeatureMapEntry;
import org.eclipse.emf.texo.model.ModelObject;

import com.thedigitalstack.model.internal.metadata.TenantModelFactory;

/**
 * The <b>{@link ModelFactory}</b> for the types of this model: shared. It contains code to create instances of {@link ModelObject} wrappers and instances of
 * EClasses and convert objects back and forth from their String (XML) representation.<br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */
public class SharedModelFactory extends TenantModelFactory implements ModelFactory {
    /**
     * @generated
     */
    private SharedModelPackage modelPackage;

    /**
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    public SharedModelFactory(SharedModelPackage modelPackage) { // NOSONAR
        this.modelPackage = modelPackage;
    }

    /**
     * Creates an instance for an {@link EClass}.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @param eClass
     *            creates a Object instance for this EClass
     * @return an object representing the eClass
     * @generated
     */
    public Object create(EClass eClass) {
        switch (eClass.getClassifierID()) {
        }
        throw new IllegalArgumentException("The EClass '" + eClass.getName() + "' is not a valid EClass for this EPackage");
    }

    /**
     * Creates an instance for an {@link EClass}.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @param eClass
     *            the EClass of the object
     * @param values
     *            the list of the object
     * @return an object container of the list
     * @generated
     */
    public Object createContainer(EClass eClass, List<?> values) {
        switch (eClass.getClassifierID()) {
        }
        throw new IllegalArgumentException("The EClass '" + eClass.getName() + "' is not a valid EClass for this EPackage");
    }

    /**
     * Wraps an object in a {@link ModelObject}.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @param eClass
     *            the EClass of the object
     * @param adaptee
     *            the object being wrapped/adapted
     * @return the wrapper {@link ModelObject}
     * @generated
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public ModelObject createModelObject(EClass eClass, Object adaptee) {
        ModelObject<Object> modelObject = null;
        switch (eClass.getClassifierID()) {
        default:
            throw new IllegalArgumentException("The EClass '" + eClass
                    + "' is not defined in this EPackage");
        }
    }

    /**
     * Creates a feature map entry instance for a certain EStructuralFeature.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @param eFeature
     *            the feature map feature
     * @return the pojo feature map entry
     * @generated
     */
    public Object createFeatureMapEntry(EStructuralFeature eFeature) {
        throw new IllegalArgumentException("The EStructuralFeature '" + eFeature + "' is not a valid feature map in this EPackage");
    }

    /**
     * Wraps a feature map entry pojo in a {@link AbstractModelFeatureMapEntry}. If the feature map entry is null then a new one is created.<br>
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @param eFeature
     *            the feature map feature of the object
     * @param adaptee
     *            the pojo feature map entry being wrapped/adapted
     * @return the wrapper {@link ModelFeatureMapEntry}
     * @generated
     */
    public ModelFeatureMapEntry<?> createModelFeatureMapEntry(EStructuralFeature eFeature, Object adaptee) {
        throw new IllegalArgumentException("The EStructuralFeature '" + eFeature + "' is not a valid feature map in this EPackage");
    }

    /**
     * Converts an instance of an {@link EDataType} to a String.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @param eDataType
     *            the {@link EDataType} defining the type
     * @param value
     *            the object to convert, if the value is null then null is returned.
     * @return the value converted
     * @generated
     */
    public Object createFromString(EDataType eDataType, String value) {

        throw new IllegalArgumentException("The EDatatype '" + eDataType + "' is not defined in this EPackage");
    }

    /**
     * Converts an instance of an {@link EDataType} to a String.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @param eDataType
     *            the {@link EDataType} defining the type
     * @param value
     *            the object to convert, if value == null then null is returned
     * @return the value converted
     * @generated
     */
    public String convertToString(EDataType eDataType, Object value) {

        throw new IllegalArgumentException("The EDatatype '" + eDataType + "' is not defined in this EPackage.");
    }

}
