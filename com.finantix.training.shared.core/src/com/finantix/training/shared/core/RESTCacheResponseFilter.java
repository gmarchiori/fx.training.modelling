package com.finantix.training.shared.core;

import java.util.Arrays;

import javax.inject.Named;
import javax.ws.rs.core.MultivaluedMap;

import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;
/**
 * Adds header: Cache-Control, Pragma end Expires for avoiding Internet Explorer caching.
 * 
 * @author renato
 *
 */
/*
 * Defect http://alm.it.fx.lan/issues/6314
 */
@Named
public class RESTCacheResponseFilter implements ContainerResponseFilter {
	private static final String CACHE_CONTROL = "Cache-Control";
	private static final String PRAGMA = "Pragma";
	private static final String EXPIRES = "Expires";

	@Override
	public ContainerResponse filter(ContainerRequest request, ContainerResponse response) {
		MultivaluedMap<String, Object> header = response.getHttpHeaders();
		if (!header.containsKey(CACHE_CONTROL) && !header.containsKey(PRAGMA) && !header.containsKey(EXPIRES)) {
			// Set standard HTTP/1.1 no-cache headers.
			header.put(CACHE_CONTROL, Arrays.<Object> asList("no-store", "no-cache", "must-revalidate"));
			// Set standard HTTP/1.0 no-cache header.
			header.putSingle(PRAGMA, "no-cache");
			header.putSingle(EXPIRES, 0);
		}
    	return response;

	}

}

