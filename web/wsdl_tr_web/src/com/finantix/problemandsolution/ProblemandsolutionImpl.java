package com.finantix.problemandsolution;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * WS example impl class
 * 
 * @author Paolo Di Patria
 *
 */
public class ProblemandsolutionImpl implements Problemandsolution {

    /*
     * No need to add comments to overridden methods, unless there is something very specific in the override (consider indicating it in the method body).
     * 
     * @see com.finantix.problemandsolution.Problemandsolution#getSolutionDetail(com.finantix.problemandsolution.GetSolutionDetailRequest)
     */
    @Override
    public GetSolutionDetailResponse getSolutionDetail(GetSolutionDetailRequest req) {
        GetSolutionDetailResponse response = new GetSolutionDetailResponse();
        
        try {
            String desc = "desc-" + req.getCode(); 
            response.setDescription(desc.substring(0, Math.min(desc.length(), 255)));
            int randomRel = (int)(10000 * Math.random());
            response.setReleasedIn("REL-" + req.getCode() + randomRel);
            response.setDeliveryDate(convertToXMLGregorianCalendar(new Date()));
            
            String productCode = req.getCode().replace("SOL", "PRO");
            response.getProblemRel().add(productCode);
        } catch (Exception e) {
            log(e);
        }
        return response;
    }
    
    
    /*
     * Convert Date to XML-ready date instance.
     */
    private XMLGregorianCalendar convertToXMLGregorianCalendar(final Date date) {
        if (date == null) return null;
        try {
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(date);

            XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH), gc.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);
            return xmlGregorianCalendar;
        } catch (DatatypeConfigurationException ex) {
            log(ex);
            return null;
        }
    }

    /*
     * Private methods might still benefit from a brief comment, unless they are simple getter/setter methods.
     */
    private void log(Exception e) {
        e.printStackTrace();
    }
}
