
package com.finantix.problemandsolution;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.finantix.problemandsolution package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetSolutionDetailRequest_QNAME = new QName("http://www.finantix.com/problemandsolution/", "getSolutionDetailRequest");
    private final static QName _GetSolutionDetailResponse_QNAME = new QName("http://www.finantix.com/problemandsolution/", "getSolutionDetailResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.finantix.problemandsolution
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetSolutionDetailResponse }
     * 
     */
    public GetSolutionDetailResponse createGetSolutionDetailResponse() {
        return new GetSolutionDetailResponse();
    }

    /**
     * Create an instance of {@link GetSolutionDetailRequest }
     * 
     */
    public GetSolutionDetailRequest createGetSolutionDetailRequest() {
        return new GetSolutionDetailRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSolutionDetailRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.finantix.com/problemandsolution/", name = "getSolutionDetailRequest")
    public JAXBElement<GetSolutionDetailRequest> createGetSolutionDetailRequest(GetSolutionDetailRequest value) {
        return new JAXBElement<GetSolutionDetailRequest>(_GetSolutionDetailRequest_QNAME, GetSolutionDetailRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSolutionDetailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.finantix.com/problemandsolution/", name = "getSolutionDetailResponse")
    public JAXBElement<GetSolutionDetailResponse> createGetSolutionDetailResponse(GetSolutionDetailResponse value) {
        return new JAXBElement<GetSolutionDetailResponse>(_GetSolutionDetailResponse_QNAME, GetSolutionDetailResponse.class, null, value);
    }

}
