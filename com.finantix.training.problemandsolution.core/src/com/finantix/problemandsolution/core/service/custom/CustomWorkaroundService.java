package com.finantix.problemandsolution.core.service.custom;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.finantix.problemandsolution.core.ProblemandsolutionModelPackage;
import com.finantix.problemandsolution.core.Workaround;
import com.finantix.problemandsolution.core.bean.WorkaroundInfoByReleaseVersionBean;
import com.finantix.problemandsolution.core.service.WorkaroundService;
import com.sun.istack.internal.NotNull;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.annotation.Nullable;
import com.thedigitalstack.model.Models;
import com.thedigitalstack.model.converter.CountConverter;
import com.thedigitalstack.model.converter.IdConverter;
import com.thedigitalstack.model.converter.IdResult;
import com.thedigitalstack.model.converter.ParamConverter;
import com.thedigitalstack.model.query.IQueryParams;
import com.thedigitalstack.model.service.Count;
import com.thedigitalstack.model.service.IOutMetadata;
import com.thedigitalstack.util.Assert;
import com.webcohesion.enunciate.metadata.rs.ResourceMethodSignature;
import com.webcohesion.enunciate.metadata.rs.ResponseHeader;
import com.webcohesion.enunciate.metadata.rs.ResponseHeaders;

@Path(com.finantix.problemandsolution.core.ProblemandsolutionModelPackage.NAME + "/" + com.finantix.problemandsolution.core.Workaround.CLASS_ID)
public class CustomWorkaroundService extends WorkaroundService {

    @GET
    @Path("fixesByReleaseVersion/{releaseVersion}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<WorkaroundInfoByReleaseVersionBean> getWorkaroundByRelaseVersion(@NotNull @PathParam("releaseVersion") String releaseVersion) {
        Assert.isNotNull(releaseVersion);

        EntityManager em = Models.getPersistenceManager().getEntityManager(ProblemandsolutionModelPackage.NS_URI);
        assert em != null;

        ProblemandsolutionModelPackage pmp = (ProblemandsolutionModelPackage) Models.getMetadataManager().getPackage(ProblemandsolutionModelPackage.NS_URI);
        assert pmp != null;

        List<WorkaroundInfoByReleaseVersionBean> resultList = null;

        StringBuilder jpqlQuery = new StringBuilder(
                "select w.acceptedByClient, p.code, p.description from problemandsolution_Workaround w left join w.problemRel p ");
        jpqlQuery.append("where w." + pmp.getSolution_ReleasedIn().getName() + " = :releasedIn");

        TypedQuery<WorkaroundInfoByReleaseVersionBean> query = em.createQuery(jpqlQuery.toString(), WorkaroundInfoByReleaseVersionBean.class);
        query.setParameter("releasedIn", releaseVersion);

        resultList = query.getResultList();

        return resultList;
    }

    @ResourceMethodSignature(output = IdResult.class)
    @Override
    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @ParamConverter(IdConverter.class)
    @NonNull
    public Workaround create(@NonNull Workaround entity) {
        return super.create(entity);
    }

    @Override
    @ResourceMethodSignature(output = Count.class)
    @DELETE
    @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @ParamConverter(CountConverter.class)
    public int removeById(@PathParam("id") @NonNull Object id) {
        return super.removeById(id);
    }

    @ResponseHeaders({ @ResponseHeader(name = "meta-totalCount", description = "Number of existing elements with the query restriction."),
            @ResponseHeader(name = "meta-partialCount", description = "Number of elements in the response."),
            @ResponseHeader(name = "meta-offset", description = "Offset in the data storage of the first element of the response."),
            @ResponseHeader(name = "meta-next", description = "Offset of the next existing element in data storage not included in the response.") })
    @ResourceMethodSignature(queryParams = { @QueryParam("l"), @QueryParam("o"), @QueryParam("by"), @QueryParam("filter") }, output = ProblemandsolutionModelPackage.WorkaroundXMLContainer.class)
    @Override
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @NonNull
    public List<Workaround> getAll(@Context @NonNull IQueryParams queryParams, @Context @Nullable IOutMetadata outMetadata) {
        return super.getAll(queryParams, outMetadata);
    }
}
