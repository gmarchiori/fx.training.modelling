package com.finantix.problemandsolution.core.service.custom;

import java.util.List;

import com.finantix.problemandsolution.core.ProblemandsolutionModelPackage;
import com.finantix.problemandsolution.core.RefSolutionType;
import com.finantix.problemandsolution.core.SolutionType;
import com.finantix.problemandsolution.core.service.RefSolutionTypeService;
import com.thedigitalstack.model.internal.metadata.MetadataTools;
import com.thedigitalstack.model.query.IPredicate;
import com.thedigitalstack.model.query.builder.QueryParamsBuilders;
import com.thedigitalstack.model.service.ServiceFactory;

/**
 * Utility class.
 * 
 * @author Paolo Di Patria
 *
 */
public class ProblemAndSolutionUtils {

    
    public static String getSolutionTypeKey(SolutionType solutionType) {
        RefSolutionType rst = getSolutionType(solutionType);
        if (rst != null) {
            return rst.getKey();
        } else {
            return null;
        }
    }
    
    
    public static RefSolutionType getSolutionType(SolutionType solutionType) {
        RefSolutionTypeService rsts = ServiceFactory.newService(RefSolutionTypeService.class);
        ProblemandsolutionModelPackage mp = (ProblemandsolutionModelPackage) MetadataTools.getTenantModelPackage(ProblemandsolutionModelPackage.NS_URI);
        IPredicate equal = QueryParamsBuilders.predicateBuilder().equal(mp.getRefSolutionType_SolutionType().getName(), solutionType);
        List<RefSolutionType> all = rsts.getAll(QueryParamsBuilders.andExpressionBuilder(equal).buildQueryParams(), null);
        if (all.size() == 1) {
            return all.get(0);
        } else {
            return null;
        }
    }
}
