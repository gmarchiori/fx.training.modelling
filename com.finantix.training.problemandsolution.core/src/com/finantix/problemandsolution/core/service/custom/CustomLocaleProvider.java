package com.finantix.problemandsolution.core.service.custom;

import java.util.Locale;

import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.authentication.spi.IUserPrincipal;
import com.thedigitalstack.core.spi.ILocaleProvider;

public class CustomLocaleProvider implements ILocaleProvider {
    @Override
    @NonNull
    public Locale getDefaultLocale() {
        return Locale.ENGLISH;
    }

    @Override
    @NonNull
    public Locale[] getLocales(@NonNull String tenantId) {
        return new Locale[]{Locale.ENGLISH, Locale.JAPANESE};
    }

    @Override
    @NonNull
    public Locale getLocale(IUserPrincipal principal) {
        // TODO Auto-generated method stub
        return Locale.ENGLISH;
    }
}
