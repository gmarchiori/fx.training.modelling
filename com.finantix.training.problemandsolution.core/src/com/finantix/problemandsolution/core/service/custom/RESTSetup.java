package com.finantix.problemandsolution.core.service.custom;

import java.util.Date;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.finantix.problemandsolution.core.SolutionType;
import com.finantix.problemandsolution.core.Workaround;
import com.finantix.problemandsolution.core.service.RefSolutionTypeService;
import com.thedigitalstack.model.service.ServiceFactory;


@Path(com.finantix.problemandsolution.core.ProblemandsolutionModelPackage.NAME + "/setup")
public class RESTSetup {

    @POST
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Path(com.finantix.problemandsolution.core.Workaround.CLASS_ID + "/{code}")
    public void setup(@PathParam("code") String code) {
        Workaround w = new Workaround();
        w.setCode("W" + code);
        w.setDescription("A workaround");
        w.setDeliveryDate(new Date());
        w.setReleasedIn("RC-TBD");
        
        RefSolutionTypeService rsts = ServiceFactory.newService(RefSolutionTypeService.class);
        String refSolutionTypeStr = null;
        if (code.startsWith("W")) {
            refSolutionTypeStr = ProblemAndSolutionUtils.getSolutionTypeKey(SolutionType.WORKAROUND);    
        } else if (code.startsWith("F")) {
            refSolutionTypeStr = ProblemAndSolutionUtils.getSolutionTypeKey(SolutionType.FIX);
        } else {
            refSolutionTypeStr = ProblemAndSolutionUtils.getSolutionTypeKey(SolutionType.UNKNOWN);
        }
        
        w.setSolutionType(refSolutionTypeStr);
    }
    
}
