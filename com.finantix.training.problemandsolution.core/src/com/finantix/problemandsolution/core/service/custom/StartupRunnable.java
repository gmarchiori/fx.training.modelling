package com.finantix.problemandsolution.core.service.custom;

import com.thedigitalstack.core.IStartupRunnable;
import com.thedigitalstack.core.Platform;
import com.thedigitalstack.core.internal.spi.IIdentityServiceFrameworkAccess;

public class StartupRunnable implements IStartupRunnable {
    @Override
    public void run() {
        IIdentityServiceFrameworkAccess acc = Platform.getAdapter(Platform.getIdentityService(), IIdentityServiceFrameworkAccess.class);
        acc.setLocaleProvider(new CustomLocaleProvider());
    }

    @Override
    public void handleException(Exception e) {
        // TODO Auto-generated method stub
        
    }
}
