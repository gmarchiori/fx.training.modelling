package com.finantix.problemandsolution.core.service.custom;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.finantix.problemandsolution.GetSolutionDetailRequest;
import com.finantix.problemandsolution.GetSolutionDetailResponse;
import com.finantix.problemandsolution.Problemandsolution;
import com.finantix.problemandsolution.core.Fix;
import com.finantix.problemandsolution.core.Problem;
import com.finantix.problemandsolution.core.ProblemandsolutionModelPackage;
import com.finantix.problemandsolution.core.Solution;
import com.finantix.problemandsolution.core.SolutionType;
import com.finantix.problemandsolution.core.Workaround;
import com.finantix.problemandsolution.core.service.FixService;
import com.finantix.problemandsolution.core.service.ProblemService;
import com.finantix.problemandsolution.core.service.WorkaroundService;
import com.finantix.training.problemandsolution.ws.consumer.ProblemAndSolutionWSFactory;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.model.metadata.TenantModelPackage;
import com.thedigitalstack.model.persistence.RunnableInTransactionAdapter;
import com.thedigitalstack.model.query.IPredicate;
import com.thedigitalstack.model.query.builder.QueryParamsBuilders;
import com.thedigitalstack.model.service.AbstractEntityService;
import com.thedigitalstack.model.service.ExecutionContext;
import com.thedigitalstack.model.service.IEntityService;
import com.thedigitalstack.model.service.ServiceFactory;
import com.thedigitalstack.restful.annotation.RSTransactional;

@Path(com.finantix.problemandsolution.core.ProblemandsolutionModelPackage.NAME + "/" + "customSolutionService")
public class CustomSolutionService extends AbstractEntityService<Solution> {

    private SolutionType type;
    
    public CustomSolutionService() {
        type = null;
    }
    
    public void setType(SolutionType type) {
        this.type = type;
    }
    

    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Solution getSolutionDetailPost(@NonNull String code) {
        return null;
    }
        
    @RSTransactional(context=RSTransactional.NONE)
     @GET
     @Path("getSolutionDetail/{code}")
     @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Solution getSolutionDetail(@PathParam("code") @NonNull String code) {
        
        ProblemandsolutionModelPackage mp = TenantModelPackage.findModelPackage(Solution.class);
        IPredicate pByCode = QueryParamsBuilders.predicateBuilder().equal(mp.getSolution_Code().getName(), code);

        List<Solution> all = new ArrayList<Solution>();
        
        WorkaroundService ws = ServiceFactory.newService(WorkaroundService.class);
        List<Workaround> allWorkarounds = ws.getAll(QueryParamsBuilders.andExpressionBuilder(pByCode).buildQueryParams(), null);
        all.addAll(allWorkarounds);
        
        FixService fs = ServiceFactory.newService(FixService.class);
        List<Fix> allFixes = fs.getAll(QueryParamsBuilders.andExpressionBuilder(pByCode).buildQueryParams(), null);
        all.addAll(allFixes);
        
        if (all == null || all.isEmpty()) {
            return null;
        }
        
        final Solution sol = all.iterator().next();
        if (sol != null) {
            Problemandsolution service = ProblemAndSolutionWSFactory.newService();
            GetSolutionDetailRequest req = new GetSolutionDetailRequest();
            req.setCode(code);
            GetSolutionDetailResponse solutionDetailFromWS = service.getSolutionDetail(req);
            mergeToSolution(sol, solutionDetailFromWS);

            ExecutionContext.runLocal(new RunnableInTransactionAdapter() {
                @Override
                public void run() {
                    if (sol instanceof Fix) {
                        Fix fix = (Fix) sol;
                        FixService fixService = ServiceFactory.newService(FixService.class);
                        fixService.update(fix);
                    } else if (sol instanceof Workaround) {
                        Fix fix = (Fix) sol;
                        FixService fixService = ServiceFactory.newService(FixService.class);
                        fixService.update(fix);
                    }
                }
            });
        }
        
        return sol;
    }
     
    /*
     * Converts a WS response in a solution
     */
    private Solution mergeToSolution(Solution sol, GetSolutionDetailResponse res) {
        sol.setDeliveryDate(res.getDeliveryDate().toGregorianCalendar().getTime());
        sol.setDescription(res.getDescription());
        sol.setReleasedIn(res.getReleasedIn());
        
        ProblemService problemService = ServiceFactory.newService(ProblemService.class);
        ProblemandsolutionModelPackage mp = TenantModelPackage.findModelPackage(Problem.class);
        
        List<String> problemRel = res.getProblemRel();
        for (String problem : problemRel) {
            IPredicate pByCode = QueryParamsBuilders.predicateBuilder().equal(mp.getProblem_Code().getName(), res.getProblemRel());
            List<Problem> problems = problemService.getAll(QueryParamsBuilders.andExpressionBuilder(pByCode).buildQueryParams(), null);
            sol.setProblemRel(problems);
        }
        return sol;
    }

    @Override
    @NonNull
    protected IEntityService<Solution> getDelegate() {
        if (type == SolutionType.FIX) {
            return ServiceFactory.newDAO(Fix.class);
        } else if (type == SolutionType.WORKAROUND) {
            return ServiceFactory.newDAO(Workaround.class);
        } else {
            return null;
        }
    }
}
