package com.finantix.problemandsolution.core.service.custom;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.finantix.problemandsolution.core.Fix;
import com.finantix.problemandsolution.core.Problem;
import com.finantix.problemandsolution.core.ProblemandsolutionModelPackage;
import com.finantix.problemandsolution.core.bean.FixInfoByReleaseVersionBean;
import com.finantix.problemandsolution.core.service.FixService;
import com.sun.istack.internal.NotNull;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.annotation.Nullable;
import com.thedigitalstack.model.Models;
import com.thedigitalstack.model.converter.CountConverter;
import com.thedigitalstack.model.converter.IdConverter;
import com.thedigitalstack.model.converter.IdResult;
import com.thedigitalstack.model.converter.ParamConverter;
import com.thedigitalstack.model.query.IQueryParams;
import com.thedigitalstack.model.service.Count;
import com.thedigitalstack.model.service.IOutMetadata;
import com.thedigitalstack.util.Assert;
import com.webcohesion.enunciate.metadata.rs.ResourceMethodSignature;
import com.webcohesion.enunciate.metadata.rs.ResponseHeader;
import com.webcohesion.enunciate.metadata.rs.ResponseHeaders;

@Path(ProblemandsolutionModelPackage.NAME + "/" + Fix.CLASS_ID)
public class CustomFixService extends FixService {

    @GET
    @Path("fixesByReleaseVersion/{releaseVersion}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<FixInfoByReleaseVersionBean> getFixByRelaseVersion(@NotNull @PathParam("releaseVersion") String releaseVersion) {
        Assert.isNotNull(releaseVersion);

        EntityManager em = Models.getPersistenceManager().getEntityManager(ProblemandsolutionModelPackage.NS_URI);
        assert em != null;
        ProblemandsolutionModelPackage pmp = (ProblemandsolutionModelPackage) Models.getMetadataManager().getPackage(ProblemandsolutionModelPackage.NS_URI);
        assert pmp != null;

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tuple> cq = cb.createTupleQuery();

        Root<Fix> root = cq.from(Fix.class);
        Join<Fix, Problem> join = root.join(pmp.getSolution_ProblemRel().getName());

        javax.persistence.criteria.Path<String> releasedInPath = root.get(pmp.getSolution_ReleasedIn().getName());
        javax.persistence.criteria.Path<String> commitRefPath = root.get(pmp.getFix_CommitRef().getName());

        javax.persistence.criteria.Path<String> problemCodePath = join.get(pmp.getProblem_Code().getName());
        javax.persistence.criteria.Path<String> problemDescriptionPath = join.get(pmp.getProblem_Description().getName());

        cq.multiselect(commitRefPath, problemCodePath, problemDescriptionPath);
        cq.where(cb.equal(releasedInPath, releaseVersion));

        TypedQuery<Tuple> query = em.createQuery(cq);
        List<Tuple> rs = query.getResultList();
        List<FixInfoByReleaseVersionBean> result = new ArrayList<FixInfoByReleaseVersionBean>();

        for (Iterator iterator = rs.iterator(); iterator.hasNext();) {
            Tuple tuple = (Tuple) iterator.next();
            FixInfoByReleaseVersionBean fixInfoByReleaseVersionBean = new FixInfoByReleaseVersionBean();
            fixInfoByReleaseVersionBean.setCommitRef((String) tuple.get(0));
            fixInfoByReleaseVersionBean.setProblemCode((String) tuple.get(1));
            fixInfoByReleaseVersionBean.setProblemDescription((String) tuple.get(2));
            result.add(fixInfoByReleaseVersionBean);
        }

        return result;
    }

    @ResourceMethodSignature(output = IdResult.class)
    @Override
    @POST
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @ParamConverter(IdConverter.class)
    @NonNull
    public Fix create(@NonNull Fix entity) {
        return super.create(entity);
    }

    @Override
    @ResourceMethodSignature(output = Count.class)
    @DELETE
    @Path("{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @ParamConverter(CountConverter.class)
    public int removeById(@PathParam("id") @NonNull Object id) {
        return super.removeById(id);
    }

    @ResponseHeaders({ @ResponseHeader(name = "meta-totalCount", description = "Number of existing elements with the query restriction."),
            @ResponseHeader(name = "meta-partialCount", description = "Number of elements in the response."),
            @ResponseHeader(name = "meta-offset", description = "Offset in the data storage of the first element of the response."),
            @ResponseHeader(name = "meta-next", description = "Offset of the next existing element in data storage not included in the response.") })
    @ResourceMethodSignature(queryParams = { @QueryParam("l"), @QueryParam("o"), @QueryParam("by"), @QueryParam("filter") }, output = ProblemandsolutionModelPackage.FixXMLContainer.class)
    @Override
    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @NonNull
    public List<Fix> getAll(@Context @NonNull IQueryParams queryParams, @Context @Nullable IOutMetadata outMetadata) {
        return super.getAll(queryParams, outMetadata);
    }
}
