package com.finantix.problemandsolution.core.service.custom;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.finantix.problemandsolution.core.Fix;
import com.finantix.problemandsolution.core.Problem;
import com.finantix.problemandsolution.core.ProblemandsolutionModelPackage;
import com.finantix.problemandsolution.core.RefSolutionType;
import com.finantix.problemandsolution.core.Solution;
import com.finantix.problemandsolution.core.SolutionType;
import com.finantix.problemandsolution.core.Workaround;
import com.finantix.problemandsolution.core.service.ProblemService;
import com.finantix.problemandsolution.core.service.RefSolutionTypeService;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.model.converter.IdConverter;
import com.thedigitalstack.model.converter.IdResult;
import com.thedigitalstack.model.converter.ParamConverter;
import com.thedigitalstack.model.internal.metadata.MetadataTools;
import com.thedigitalstack.model.query.IPredicate;
import com.thedigitalstack.model.query.builder.QueryParamsBuilders;
import com.thedigitalstack.model.service.ServiceFactory;
import com.thedigitalstack.restful.annotation.RSTransactional;
import com.webcohesion.enunciate.metadata.rs.ResourceMethodSignature;

@Path(com.finantix.problemandsolution.core.ProblemandsolutionModelPackage.NAME + "/" + com.finantix.problemandsolution.core.Problem.CLASS_ID)
public class CustomProblemService extends ProblemService {
    
     @ResourceMethodSignature(output = IdResult.class)
    @Override
     @POST
     @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
     @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
     @ParamConverter(IdConverter.class)
    @NonNull
    @RSTransactional(context = RSTransactional.TRANSACTIONAL)
    public Problem create(@NonNull Problem entity) {
        
        Problem aProblem = super.create(entity);
        
        final CustomSolutionService newService = ServiceFactory.newService(CustomSolutionService.class);
        
        Solution aSolution = getSolution(aProblem);
        newService.create(aSolution);
        return aProblem;
    }

    private @NonNull Solution getSolution(@NonNull Problem aProblem) {
        Solution aSolution = null;
        if (aProblem.getCode().startsWith("W")) {
            aSolution = new Workaround();
            aSolution.setCode("W" + aProblem.getCode());
            aSolution.setSolutionType(ProblemAndSolutionUtils.getSolutionTypeKey(SolutionType.WORKAROUND));
        } else if (aProblem.getCode().startsWith("F")) {
            aSolution = new Fix();
            aSolution.setCode("F" + aProblem.getCode());

        }
        if (aProblem.getDescription() != null) {
            aSolution.setDescription("Solution for " + aProblem.getDescription());
        }
        
        if (aSolution instanceof Fix) {
            Fix fix = (Fix) aSolution;
            fix.getProblemRel().add(aProblem);
        } else if (aSolution instanceof Workaround) {
            Workaround wa = (Workaround) aSolution;
            wa.getProblemRel().add(aProblem);
        }
        
        return aSolution;
    }
}