package com.finantix.problemandsolution.core.service;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.thedigitalstack.core.Platform;
import com.thedigitalstack.model.service.ServiceFactory;
import com.thedigitalstack.restful.annotation.RSTransactional;
import com.thedigitalstack.util.RunnableAdapter;

@Path("training/solution/export")
@RolesAllowed("BATCH")
public class RESTSolutionExportService {

	@Path("start")
    @GET
    @RSTransactional(context = RSTransactional.NONE)
    public Response startExport() {
        Platform.getDistributedExecutor().asyncExecuteExclusive(new RunnableAdapter() {
            @Override
            public void run() {
                try {
                	ServiceFactory.newService(SolutionExportService.class).syncRunExports();
                } catch (Exception e) {
                    Platform.getLogger(RESTSolutionExportService.class).error("Error performing solutiondata export", e);
                }
            }
        }, getClass().getName());
        return Response.ok().entity("Export Started").build();
    }
    
}
