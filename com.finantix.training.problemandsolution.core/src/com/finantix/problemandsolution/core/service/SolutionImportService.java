package com.finantix.problemandsolution.core.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;

import com.thedigitalstack.configuration.Configurator;
import com.thedigitalstack.core.Platform;
import com.thedigitalstack.core.audit.AuditEvent;
import com.thedigitalstack.impexp.IImportJob;
import com.thedigitalstack.impexp.IImportService;
import com.thedigitalstack.impexp.spi.AuditConsts;
import com.thedigitalstack.model.service.ServiceFactory;

/**
 * Solution Import Service.
 * 
 * @author Paolo Di Patria
 *
 */
public class SolutionImportService {

    /*
     * For Auditing purposes
     */
    private static final String SOLUTION_IMPORT_SOURCE = "SOLUTION-IMPORT";
    
    /*
     * Must match contrib.xml "com.thedigitalstack.impexp.imports" extension point id
     */
    public static final String jobId = "solutiondata_import";
    
    /**
     * Name of the upload folder.
     */
    public final static String FOLDER_UPLOAD = "upload";
    /**
     * Name of the temporary (chunk) folder.
     */
    public final static String FOLDER_WORK = "work";
    /**
     * Name of the backup folder.
     */
    public final static String FOLDER_BACKUP = "backup";
    
    
    private Logger logger;

    public void syncRunImports() {
        logger = Platform.getLogger(SolutionImportService.class);
        
        List<ImportResult> result = new ArrayList<SolutionImportService.ImportResult>();
        String context = SOLUTION_IMPORT_SOURCE + "-" + UUID.randomUUID().toString();

        ISolutionDataImportConfiguration sdiCfg = null;

        // check for proper configuration
        try {
            sdiCfg = new Configurator().get(ISolutionDataImportConfiguration.class);
            sdiCfg.getRootFolder();
        } catch (RuntimeException e) {
            logger.error("Import cannot start due to improper configuration. Check configuration files.");
            throw e;
        }
        
        
        Platform.getAuditService().audit(new AuditEvent(SOLUTION_IMPORT_SOURCE, context, AuditConsts.ACTION_START, null, null, null));

        try {
            syncRunImport(SOLUTION_IMPORT_SOURCE, "SOLUTION-IMPORT", sdiCfg.getRootFolder(), result);
        } catch (IOException e) {
            logger.error("Import IO issue", e);
            throw new SolutionImportRuntimeException(e);
        }


        result = Collections.unmodifiableList(result);
    }
    
    /*
     * 
     */
    private List<ImportResult> syncRunImport(String solutionImportSource, String type, String rootFolder, List<ImportResult> result) throws IOException {
        logger.info("Starting {} import", type);
        File cRootFolder = checkRootFolder(rootFolder);
        List<ImportResult> jobs = synchPerformImport(cRootFolder, type, cRootFolder.getCanonicalPath() + File.separator + "upload");
        backup(cRootFolder, jobs);
        cleanupWorkDir(cRootFolder);

        List<String> instanceIds = new ArrayList<String>();
        if (jobs != null) {
            for (ImportResult r : jobs) {
                instanceIds.add(r.getJob().getInstanceId());
            }
        }
        logger.info("{} import jobs completed {}", type, instanceIds);
        return jobs;
    }
    
    
    private void backup(File cRootFolder, List<ImportResult> jobs) {
    	File backup = new File(cRootFolder, FOLDER_BACKUP);
        if (!backup.exists()) {
            if (!backup.mkdirs()) {
                logger.error("Backup folder {} is missing and cannot be created, backup skipped", backup);
                return;
            }
        } else if (!backup.canWrite()) {
            logger.error("Insufficient permissions, cannot write to backup folder {}, backup skipped", backup);
            return;
        }

        if (jobs != null) {
            for (ImportResult jobResult : jobs) {
                File file = jobResult.getProcessedFile();
                try {
                    FileUtils.copyFileToDirectory(file, backup);
                } catch (IOException e) {
                    logger.error("Error copying input file {} to backup folder {}", file, backup);
                }
                if (!FileUtils.deleteQuietly(file)) {
                    logger.error("Could not delete input file {}", file);
                }
            }
        }
		
	}

	/*
     * Implement this.
     */
    private void cleanupWorkDir(File cRootFolder) {
    	File work = new File(cRootFolder, FOLDER_WORK);
        try {
            FileUtils.deleteDirectory(work);
        } catch (IOException e) {
            logger.info("Cannot delete work folder {}", work, e);
        }
        
    }
    
    
    private InputStream getImportInputStream(File file) {
        try {
            if (!file.canRead()) {
                throw new SolutionImportRuntimeException("Insufficient permissions for reading " + file.getCanonicalPath() + ", skipping");
            }
            return new BufferedInputStream(new FileInputStream(file));
        } catch (Exception e) {
            throw new SolutionImportRuntimeException(e);
        }
    }
    

    /**
     * Import core routine.
     * 
     * @param rootFolder
     * @param importType
     * @param uploadFolderPath
     * @return
     */
    protected List<ImportResult> synchPerformImport(File rootFolder, String importType, String uploadFolderPath) {
        List<File> files = findImportFiles(rootFolder, importType, uploadFolderPath);
        if (files == null || files.isEmpty()) {
            logger.info("No files found in folder {}", rootFolder);
            return Collections.emptyList();
        } 
        List<ImportResult> result = new ArrayList<ImportResult>();
        
        IImportService service = ServiceFactory.newService(IImportService.class);
        
        for (File file : files) {
            InputStream is = null;
            IImportJob job = null;
            try {
                is = getImportInputStream(file);
            } catch (Exception e) {
                logger.error("File access issue", e);
                continue;
            }

            // job is not available until runImport is called 
            final ImportResult dailyResult = new ImportResult(job, file);
            dailyResult.markStarted();
            try {
                job = service.runImport(is, jobId);
                dailyResult.job = job;
                logger.info("Import job of type {} started, instance id is {}, input file is {}", job.getJobId(), job.getInstanceId(), dailyResult.getImportFileCanonicalPath());
                result.add(dailyResult);
                job.waitForCompletion();
            } catch (InterruptedException ie) {
                logger.info("Import {} interrupted - jobId: {} - file: {}", job.getJobId(), job.getInstanceId(), dailyResult.getImportFileCanonicalPath());
            } catch (ExecutionException ee) {
                logger.error("Import {} threw an exception - jobId: {} - file: {}", job.getJobId(), job.getInstanceId(), dailyResult.getImportFileCanonicalPath(), ee);
            } finally {
                dailyResult.markCompleted();
                IOUtils.closeQuietly(is);
            }
        }
        
        return result;
    }
    
    
    /*
     * Implement this.
     */
    private List<File> findImportFiles(File rootFolder, String importType, String uploadFolderPath) {
        List<File> files = new ArrayList<File>();
        File importFile = new File(rootFolder.getAbsolutePath() + File.separator + FOLDER_UPLOAD + File.separator + "solution.xml");
        files.add(importFile);
        return files;
    }

    private File checkRootFolder(String rootFolder) {
        File rootFolderObj = new File(rootFolder);
        if (rootFolderObj.exists() && rootFolderObj.isDirectory() && rootFolderObj.canWrite()) {
            return rootFolderObj;
        } else {
            throw new SolutionImportRuntimeException("Root folder not existing, not a directory or not writeable, please check " + rootFolderObj.getPath());
        }
    }

    static class ImportResult {
        private IImportJob job;
        
        private File processedFile;
        
        private long durationMillis;
        
        transient private long start;
        

        protected ImportResult(IImportJob job, File processedFile) {
            super();
            this.job = job;
            this.processedFile = processedFile;
        }

        public File getProcessedFile() {
            return processedFile;
        }

        public IImportJob getJob() {
            return job;
        }
        
        private void markStarted() {
            start = System.currentTimeMillis();
        }
        
        private void markCompleted() {
            durationMillis = System.currentTimeMillis() -start;
        }
        
        public long getDurationMillis() {
            return durationMillis;
        }
        
        public String getImportFileCanonicalPath() {
            try {
                return processedFile.getCanonicalPath();
            } catch (Exception e) {
                // !! exception 'lost'
                return processedFile.getPath();
            }
        }
    }

}
