package com.finantix.problemandsolution.core.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;

import org.apache.commons.io.IOUtils;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.slf4j.Logger;

import com.finantix.problemandsolution.core.Fix;
import com.finantix.problemandsolution.core.Problem;
import com.finantix.problemandsolution.core.ProblemandsolutionModelPackage;
import com.finantix.problemandsolution.core.RefSolutionType;
import com.finantix.problemandsolution.core.Solution;
import com.finantix.problemandsolution.core.SolutionType;
import com.finantix.problemandsolution.core.Workaround;
import com.finantix.problemandsolution.core.service.bean.ProblemBean;
import com.finantix.problemandsolution.core.service.bean.SolutionDataBean;
import com.thedigitalstack.configuration.Configurator;
import com.thedigitalstack.core.Platform;
import com.thedigitalstack.core.audit.AuditEvent;
import com.thedigitalstack.core.audit.IAuditService;
import com.thedigitalstack.impexp.spi.AuditConsts;
import com.thedigitalstack.model.Models;
import com.thedigitalstack.model.service.ServiceFactory;

public class SolutionExportService {
	private static Logger logger = Platform.getLogger(SolutionExportService.class);
	static final String SOLUTION_EXPORT_SOURCE = "SolutionDataExport";
	
	private final static XMLOutputFactory FACTORY = XMLOutputFactory.newInstance();
    {
        FACTORY.setProperty(XMLOutputFactory.IS_REPAIRING_NAMESPACES, true);
    }
    private final static XMLEventFactory EVENT_FACTORY = XMLEventFactory.newInstance();

    private final static QName SOLUTIONS = QName.valueOf("solutions");

    private static JAXBContext CONTEXT;
    {
        try {
            CONTEXT = JAXBContextFactory.createContext(new Class<?>[] { SolutionDataBean.class }, Collections.EMPTY_MAP);
        } catch (JAXBException e) {
        }
    }
    
	public void syncRunExports() {
		String context = SOLUTION_EXPORT_SOURCE + "-"+UUID.randomUUID().toString();
        
        ISolutionDataExportConfiguration seCfg = null;
        
        // check for proper configuration
        try {
        	seCfg = new Configurator().get(ISolutionDataExportConfiguration.class);
        	seCfg.getRootFolder();
        } catch (RuntimeException e) {
            logger.error("Export cannot start due to improper configuration. Check configuration files.");
            throw e;
        }
        
        try {
            
            Platform.getAuditService().audit(new AuditEvent(SOLUTION_EXPORT_SOURCE, context, AuditConsts.ACTION_START, null, null, null));
            
            try {
            	exportSolutionData(seCfg);
			} catch (XMLStreamException e) {
				e.printStackTrace();
			} catch (JAXBException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
        } finally {
            
        }

        logger.info("Daily export completed");

        Map<String, Object> auditEvent = new LinkedHashMap<String, Object>();
        Platform.getAuditService().audit(
                new AuditEvent(SOLUTION_EXPORT_SOURCE, context, AuditConsts.ACTION_JOB_COMPLETED, null, auditEvent, null));
        
		
	}

	private void exportSolutionData(ISolutionDataExportConfiguration seCfg)  throws XMLStreamException, JAXBException, IOException {
        final IAuditService auditService = Platform.getAuditService();

        OutputStream os = null;
        final long start = System.currentTimeMillis();
        
        final String ctx = "SolutionDataExport-"+UUID.randomUUID().toString();
        
        Exception failure = null;
        long totalRecords = 0;
        
        String exportSource = "SolutionDataExport";
		try {
            AuditEvent audit = new AuditEvent(exportSource, ctx, AuditConsts.ACTION_START, null, null, null);            
            auditService.audit(audit);
            
            os = getOutputStream(seCfg);
            totalRecords = exportSolutionData(os, seCfg);

        } catch (XMLStreamException e) {
            failure = e;
            throw e;
        } catch (JAXBException e) {
            failure = e;
            throw e;
        } catch (IOException e) {
            failure = e;
            throw e;
        } finally {
            Map<String, Object> result = new HashMap<String, Object>();
            result.put("instance", ctx);
            result.put("durationMillis", System.currentTimeMillis()-start);
            result.put("totalRecords", totalRecords);
            AuditEvent event = null;
            if (failure == null) { 
                // all fine
                event = new AuditEvent(exportSource, ctx, AuditConsts.ACTION_JOB_COMPLETED, null, result, null);
            } else {
                // failed
                result.put("exception", failure);
                event = new AuditEvent(exportSource, ctx, AuditConsts.ACTION_JOB_COMPLETED, null, null, result);
            }
            auditService.audit(event);
            IOUtils.closeQuietly(os);
        }
	}

	private long exportSolutionData(OutputStream os, ISolutionDataExportConfiguration seCfg) throws XMLStreamException, JAXBException {
        if (CONTEXT == null) {
            throw new JAXBException("Cannot create JAXB Context");
        }

        if (os instanceof BufferedOutputStream == false) {
            os = new BufferedOutputStream(os);
        }

        Date date = Calendar.getInstance().getTime();

        XMLEventWriter writer = FACTORY.createXMLEventWriter(os, "UTF-8");

        writer.add(EVENT_FACTORY.createStartDocument());

        writer.add(EVENT_FACTORY.createStartElement(SOLUTIONS, null, null));

        long total = doExportSolutions(writer, seCfg);

        writer.add(EVENT_FACTORY.createEndElement(SOLUTIONS, null));

        writer.flush();

        return total;
	}

	private long doExportSolutions(XMLEventWriter writer, ISolutionDataExportConfiguration seCfg) throws JAXBException {
		long total = 0;
        int iteration = 0;
        int chunkSize = seCfg.getChunkSize();

        EntityManager em = Models.getPersistenceManager().getEntityManager(Solution.class);
        ProblemandsolutionModelPackage pmp = (ProblemandsolutionModelPackage) Models.getMetadataManager().getPackage(ProblemandsolutionModelPackage.NS_URI);
        
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Solution> cq = cb.createQuery(Solution.class);
        Root<Solution> root = cq.from(Solution.class);
        cq.orderBy(cb.asc(root.get(pmp.getSolution_Id().getName())));
        cq.select(root);
        TypedQuery<Solution> query = em.createQuery(cq);
        
        List<Solution> resultList = Collections.emptyList();
        Marshaller marshaller = CONTEXT.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
        try {
            do {
            	query.setFirstResult(iteration * chunkSize);
            	query.setMaxResults(chunkSize);
                resultList = query.getResultList();
                iteration++;
                if (!resultList.isEmpty()) {
                    doExportSolutions(marshaller, writer, convertSolutionToSolutionDataBean(resultList));
                }
                total = total + resultList.size();
            } while (resultList.size() == chunkSize);

        } finally {
        	em.close();
        }
        
        return total;
	}

	private void doExportSolutions(Marshaller marshaller,
			XMLEventWriter writer,
			List<SolutionDataBean> solutionDataBeanList)  throws JAXBException {
		for (SolutionDataBean solutionDataBean : solutionDataBeanList) {
            marshaller.marshal(solutionDataBean, writer);
        }
		
	}

	private List<SolutionDataBean> convertSolutionToSolutionDataBean(List<Solution> resultList) {
		RefSolutionTypeService srv = ServiceFactory.newService(RefSolutionTypeService.class);
		List<RefSolutionType> all = srv.getAll();
		Map<String, SolutionType> solutionTypeByKey = new HashMap<String, SolutionType>();
		
		for (Iterator iterator = all.iterator(); iterator.hasNext();) {
			RefSolutionType refSolutionType = (RefSolutionType) iterator.next();
			solutionTypeByKey.put(refSolutionType.getKey(), refSolutionType.getSolutionType());
		}
		List<SolutionDataBean> list = new ArrayList<SolutionDataBean>();
		for (Iterator iterator = resultList.iterator(); iterator.hasNext();) {
			Solution solution = (Solution) iterator.next();
			if(solution.getSolutionType()!=null){
				SolutionType solutionType = solutionTypeByKey.get(solution.getSolutionType());
				if(solutionType!=null && solutionType==SolutionType.FIX){
					Fix fix = (Fix)solution;
					SolutionDataBean solutionDataBean = new SolutionDataBean();
					fillCommonData(solution, solutionDataBean, solutionType);
					solutionDataBean.setCommitRef(fix.getCommitRef());
					list.add(solutionDataBean);
				} else if(solutionType!=null && solutionType==SolutionType.WORKAROUND){
					Workaround workaround = (Workaround) solution;
					SolutionDataBean solutionDataBean = new SolutionDataBean();
					fillCommonData(solution, solutionDataBean, solutionType);
					solutionDataBean.setAcceptedByClient(workaround.isAcceptedByClient());
					list.add(solutionDataBean);
				}
			}
		}
		return list;
	}

	private void fillCommonData(Solution solution,
			SolutionDataBean solutionDataBean, SolutionType solutionType) {
		solutionDataBean.setCode(solution.getCode());
		solutionDataBean.setSolutionType(solutionType);
		solutionDataBean.setDeliveryDate(solution.getDeliveryDate());
		solutionDataBean.setDescription(solution.getDescription());
		solutionDataBean.setReleasedIn(solution.getReleasedIn());
		List<Problem> problemRel = solution.getProblemRel();
		List<ProblemBean> list = new ArrayList<ProblemBean>();
		
		for (Iterator iterator = problemRel.iterator(); iterator.hasNext();) {
			Problem problem = (Problem) iterator.next();
			ProblemBean problemBean = new ProblemBean();
			problemBean.setCode(problem.getCode());
			problemBean.setDescription(problem.getDescription());
			problemBean.setLastUpdated(problem.getLastUpdated());
			list.add(problemBean);
		}
		solutionDataBean.setProblemRel(list);
	}

	/**
     * Opens default outputstream for writing, if not provided as one of the
     * public methods parameters.
     * 
     * @param cfg
     *            the configuration
     * @return the output stream
     * @throws IOException
     *             in case of errors.
     */
    protected OutputStream getOutputStream(ISolutionDataExportConfiguration cfg) throws IOException {
        File folder = new File(cfg.getRootFolder());
        if (!folder.exists()) {
            if (!folder.mkdirs()) {
                throw new IOException("Root folder does not exist and cannot be created: " + cfg.getRootFolder());
            }
        }

//        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
        File file = new File(folder, "solution.xml");
        return new FileOutputStream(file);
    }
}
