package com.finantix.problemandsolution.core.service;

import com.thedigitalstack.configuration.IConfiguration;
import com.thedigitalstack.configuration.Location;
import com.thedigitalstack.impexp.IImportConfiguration;

@com.thedigitalstack.configuration.Configuration(locations = { Location.File }, resource = "solutiondata-import.properties")
public interface ISolutionDataImportConfiguration extends IImportConfiguration, IConfiguration {

    /**
     * The root folder of the import process.
     * @return the root folder
     */
    public String getRootFolder();
}
