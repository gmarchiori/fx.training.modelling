package com.finantix.problemandsolution.core.service;

import com.thedigitalstack.impexp.spi.XMLInputSplitter;

/**
 * 
 * @author Paolo Di Patria
 *
 */
public class SolutionDataInputSplitter extends XMLInputSplitter {
   
    /**
     * 
     */
    public SolutionDataInputSplitter() {
        super();
        setRecordNodeAncestors(SolutionDataImportWorker.ANCESTORS);
    }
}
