package com.finantix.problemandsolution.core.service.bean;

import com.finantix.problemandsolution.core.Fix;
import com.finantix.problemandsolution.core.Solution;
import com.finantix.problemandsolution.core.SolutionType;
import com.finantix.problemandsolution.core.Workaround;
import com.thedigitalstack.core.IAdapterFactory;

public class SolutionDataBeanAdapterFactory implements IAdapterFactory {

    @Override
    public <T> T getAdapter(Object adaptableObject, Class<T> adapterType) {
        if (adaptableObject instanceof SolutionDataBean) {
            SolutionDataBean sdb = (SolutionDataBean) adaptableObject;
            if (sdb.getSolutionType() == SolutionType.FIX) {
                return (T) new Fix();
            } else if (sdb.getSolutionType() == SolutionType.WORKAROUND) {
                return (T) new Workaround();
            }
        }
        return null;
    }

    @Override
    public Class<Object>[] getAdapterList() {
        return new Class[] { Solution.class };
    }

}
