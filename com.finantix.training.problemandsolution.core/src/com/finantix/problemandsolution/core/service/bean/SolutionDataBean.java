package com.finantix.problemandsolution.core.service.bean;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.finantix.problemandsolution.core.SolutionType;

/**
 * 
 * @author Paolo Di Patria
 *
 */
@XmlRootElement
public class SolutionDataBean {
	private SolutionType solutionType;
	
	private String code;
	
	private String description;
	
	private Date deliveryDate;
	
	private String releasedIn;
	
	private List<ProblemBean> problemRel;
	
	private String commitRef;
	
	private boolean acceptedByClient;

	public SolutionType getSolutionType() {
		return solutionType;
	}

	public void setSolutionType(SolutionType solutionType) {
		this.solutionType = solutionType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getReleasedIn() {
		return releasedIn;
	}

	public void setReleasedIn(String releasedIn) {
		this.releasedIn = releasedIn;
	}

	public List<ProblemBean> getProblemRel() {
		return problemRel;
	}

	public void setProblemRel(List<ProblemBean> problemRel) {
		this.problemRel = problemRel;
	}

	public String getCommitRef() {
		return commitRef;
	}

	public void setCommitRef(String commitRef) {
		this.commitRef = commitRef;
	}

	public boolean isAcceptedByClient() {
		return acceptedByClient;
	}

	public void setAcceptedByClient(boolean acceptedByClient) {
		this.acceptedByClient = acceptedByClient;
	}
	
}
