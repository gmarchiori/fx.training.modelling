package com.finantix.problemandsolution.core.service;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.thedigitalstack.core.Platform;
import com.thedigitalstack.model.service.ServiceFactory;
import com.thedigitalstack.util.RunnableAdapter;

@Path("training/solution/import")
@RolesAllowed("BATCH")
public class RESTSolutionImportService {

    /**
     * 
     * @return
     */
    @Path("start")
    @GET
    public Response startImport() {
        Platform.getDistributedExecutor().asyncExecuteExclusive(new RunnableAdapter() {
            @Override
            public void run() {
                SolutionImportService srv = ServiceFactory.newService(SolutionImportService.class);
                srv.syncRunImports();
            }
        }, RESTSolutionImportService.class.getName());

        return Response.ok().entity("Import started").build();
    }
    
}
