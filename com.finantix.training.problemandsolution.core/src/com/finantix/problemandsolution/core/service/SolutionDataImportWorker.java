package com.finantix.problemandsolution.core.service;

import java.util.List;

import com.finantix.problemandsolution.core.ProblemandsolutionModelPackage;
import com.finantix.problemandsolution.core.Solution;
import com.finantix.problemandsolution.core.SolutionType;
import com.finantix.problemandsolution.core.dao.FixDao;
import com.finantix.problemandsolution.core.dao.WorkaroundDao;
import com.finantix.problemandsolution.core.service.bean.SolutionDataBean;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.core.Platform;
import com.thedigitalstack.impexp.ChunkResult;
import com.thedigitalstack.impexp.IImportConfiguration;
import com.thedigitalstack.impexp.spi.AbstractJAXBImportWorker;
import com.thedigitalstack.model.metadata.TenantModelPackage;
import com.thedigitalstack.model.query.IPredicate;
import com.thedigitalstack.model.query.IQueryParams;
import com.thedigitalstack.model.query.builder.QueryParamsBuilders;
import com.thedigitalstack.model.service.IDao;
import com.thedigitalstack.model.service.ServiceFactory;

public class SolutionDataImportWorker extends AbstractJAXBImportWorker<String, SolutionDataBean> {
   
    public static final String ANCESTORS = "solutions";

//	public SolutionDataImportWorker(Logger logger, Class<SolutionDataBean> recordClass) {
//        super(Platform.getLogger(SolutionDataImportWorker.class), recordClass);
//        setRecordNodeAncestors(ANCESTORS);
//    }
	
	public SolutionDataImportWorker() {
        super(SolutionDataBean.class);
        setRecordNodeAncestors(ANCESTORS);
    }

    @Override
    @NonNull
    protected ChunkResult doProcessChunk(@NonNull String instanceId, @NonNull IImportConfiguration configuration, @NonNull String chunkIdentifier,
            @NonNull List<SolutionDataBean> objects) {
        ChunkResult chunkResult = new ChunkResult(chunkIdentifier);
        
        ProblemandsolutionModelPackage mp = TenantModelPackage.findModelPackage(Solution.class);
        
        for (SolutionDataBean solutionDataBean : objects) {
            IDao<Solution> dao = null; 
            
            if (solutionDataBean.getSolutionType() == SolutionType.WORKAROUND) {
                dao = ServiceFactory.newDAO(WorkaroundDao.class);
            } else if (solutionDataBean.getSolutionType() == SolutionType.FIX) {
                dao = ServiceFactory.newDAO(FixDao.class);
            }
            
            IPredicate pEqByCode = QueryParamsBuilders.predicateBuilder().equal(mp.getSolution_Code().getName(), solutionDataBean.getCode());
            IQueryParams qpSolutionsByProdCode = QueryParamsBuilders.andExpressionBuilder(pEqByCode).buildQueryParams();
            
            List<Solution> solutions = dao.getAll(qpSolutionsByProdCode, null);
            
            Solution adapted = Platform.getAdapter(solutionDataBean, Solution.class);
            
            switch (solutions.size()) {
                case 0:
                    dao.create(adapted);
                    break;
                case 1:
                    adapted.setUpdateVersion(solutions.get(0).getUpdateVersion());
                    dao.update(adapted);
                    break;
                default:
                    // TODO: add exception
            }
        }
        
        chunkResult.success();
        
        return chunkResult;
    }
}
