package com.finantix.problemandsolution.core.service;


/**
 * Solution Import specific runtime exception.
 * 
 * @author Paolo Di Patria
 *
 */
public class SolutionImportRuntimeException extends RuntimeException {

    /**
     * 
     * @param message
     */
    public SolutionImportRuntimeException(String message) {
        super(message);
    }

    public SolutionImportRuntimeException(Exception e) {
        super(e);
    }

}
