package com.finantix.problemandsolution.core.bean;

public class WorkaroundInfoByReleaseVersionBean {
    
    private boolean isAcceptedByClient;
    private String problemCode;
    private String problemDescription;
    
	public String getProblemCode() {
        return problemCode;
    }
    public void setProblemCode(String problemCode) {
        this.problemCode = problemCode;
    }
    public String getProblemDescription() {
        return problemDescription;
    }
    public void setProblemDescription(String problemDescription) {
        this.problemDescription = problemDescription;
    }
    public boolean isAcceptedByClient() {
        return isAcceptedByClient;
    }
    public void setAcceptedByClient(boolean isAcceptedByClient) {
        this.isAcceptedByClient = isAcceptedByClient;
    }
}
