package com.finantix.problemandsolution.core.bean;

public class FixInfoByReleaseVersionBean {
    private String commitRef;
    private String problemCode;
    private String problemDescription;
    public String getCommitRef() {
        return commitRef;
    }
    public void setCommitRef(String commitRef) {
        this.commitRef = commitRef;
    }
    public String getProblemCode() {
        return problemCode;
    }
    public void setProblemCode(String problemCode) {
        this.problemCode = problemCode;
    }
    public String getProblemDescription() {
        return problemDescription;
    }
    public void setProblemDescription(String problemDescription) {
        this.problemDescription = problemDescription;
    }
    
}
