package com.finantix.problemandsolution.core;

import java.util.Locale;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.eclipse.persistence.annotations.Index;
import org.eclipse.persistence.annotations.Indexes;
import org.eclipse.persistence.annotations.Multitenant;
import org.eclipse.persistence.annotations.TenantDiscriminatorColumn;
import org.eclipse.persistence.annotations.TenantDiscriminatorColumns;
import org.eclipse.persistence.annotations.VirtualAccessMethods;
import org.eclipse.persistence.oxm.annotations.XmlVirtualAccessMethods;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.annotation.Nullable;
import com.thedigitalstack.model.l10n.LocalizedEntity;
import com.thedigitalstack.model.l10n.adapter.L10NFieldXmlAdapter;

/**
 * A representation of the model object '<em><b>WorkaroundNotes</b></em>'.<br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */
@Entity(name = "problemandsolution_WorkaroundNotes")
@Multitenant()
@TenantDiscriminatorColumns({ @TenantDiscriminatorColumn(name = "tenant_id") })
@Indexes({
		@Index(columnNames = { "lang", "field" }, name = "problemandsolution_WorkaroundNotes_l10n", table = "problemandsolution_WorkaroundNotes_l10nvalues"),
		@Index(columnNames = { "lang" }, name = "problemandsolution_WorkaroundNotes_l10n_lang", table = "problemandsolution_WorkaroundNotes_l10nvalues"),
		@Index(columnNames = { "field" }, name = "problemandsolution_WorkaroundNotes_l10n_field", table = "problemandsolution_WorkaroundNotes_l10nvalues") })
@XmlVirtualAccessMethods()
@XmlRootElement(namespace = ProblemandsolutionModelPackage.NS_URI)
@VirtualAccessMethods()
public class WorkaroundNotes extends LocalizedEntity

{
	/**
	 * @generated
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Id()
	@GeneratedValue(generator = "problemandsolution_workaroundnotes")
	@SequenceGenerator(name = "problemandsolution_workaroundnotes", sequenceName = "problemandsolution_workaroundnotes_seq")
	private long id = 0;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Basic()
	private long workaroundId = 0;

	/**
	 * Identifier of the class.
	 * 
	 * @generated
	 */
	public static final String CLASS_ID = "workaroundNotess";

	/**
	 * The feature id for the '{@link WorkaroundNotes <em>title</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static final String LOCALIZED_FIELD_TITLE = "title";

	/**
	 * Returns the value of '<em><b>title</b></em>' feature. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>title</b></em>' feature
	 * @generated
	 */
	@XmlJavaTypeAdapter(L10NFieldXmlAdapter.class)
	@Nullable
	public String getTitle() {
		return getField(LOCALIZED_FIELD_TITLE);
	}

	/**
	 * Returns the value of '<em><b>title</b></em>' feature for a given locale.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>title</b></em>' feature
	 * @generated
	 */
	@Nullable
	public String getTitle(@NonNull Locale locale) {
		return getField(locale, LOCALIZED_FIELD_TITLE);
	}

	/**
	 * Sets the '{@link WorkaroundNotes#getTitle() <em>title</em>}' feature.
	 * 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param newTitle
	 *            the new value of the '{@link WorkaroundNotes#getTitle() title}
	 *            ' feature.
	 * @generated
	 */
	public void setTitle(String newTitle) {
		setField(LOCALIZED_FIELD_TITLE, newTitle);
	}

	/**
	 * Sets the '{@link WorkaroundNotes#getTitle() <em>title</em>}' feature for
	 * a given locale.
	 * 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param newTitle
	 *            the new value of the '{@link WorkaroundNotes#getTitle() title}
	 *            ' feature.
	 * @generated
	 */
	public void setTitle(@NonNull Locale locale, String newTitle) {
		setField(locale, LOCALIZED_FIELD_TITLE, newTitle);
	}

	/**
	 * The feature id for the '{@link WorkaroundNotes <em>content</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static final String LOCALIZED_FIELD_CONTENT = "content";

	/**
	 * Returns the value of '<em><b>content</b></em>' feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>content</b></em>' feature
	 * @generated
	 */
	@XmlJavaTypeAdapter(L10NFieldXmlAdapter.class)
	@Nullable
	public String getContent() {
		return getField(LOCALIZED_FIELD_CONTENT);
	}

	/**
	 * Returns the value of '<em><b>content</b></em>' feature for a given
	 * locale. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>content</b></em>' feature
	 * @generated
	 */
	@Nullable
	public String getContent(@NonNull Locale locale) {
		return getField(locale, LOCALIZED_FIELD_CONTENT);
	}

	/**
	 * Sets the '{@link WorkaroundNotes#getContent() <em>content</em>}' feature.
	 * 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param newContent
	 *            the new value of the '{@link WorkaroundNotes#getContent()
	 *            content}' feature.
	 * @generated
	 */
	public void setContent(String newContent) {
		setField(LOCALIZED_FIELD_CONTENT, newContent);
	}

	/**
	 * Sets the '{@link WorkaroundNotes#getContent() <em>content</em>}' feature
	 * for a given locale.
	 * 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param newContent
	 *            the new value of the '{@link WorkaroundNotes#getContent()
	 *            content}' feature.
	 * @generated
	 */
	public void setContent(@NonNull Locale locale, String newContent) {
		setField(locale, LOCALIZED_FIELD_CONTENT, newContent);
	}

	/**
	 * Returns the value of '<em><b>id</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>id</b></em>' feature
	 * @generated
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the '{@link WorkaroundNotes#getId() <em>id</em>}' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newId
	 *            the new value of the '{@link WorkaroundNotes#getId() id}'
	 *            feature.
	 * @generated
	 */
	public void setId(long newId) {
		id = newId;

	}

	/**
	 * Returns the value of '<em><b>workaroundId</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>workaroundId</b></em>' feature
	 * @generated
	 */
	@XmlJavaTypeAdapter(ProblemandsolutionModelPackage.WorkaroundWeakAdapter.class)
	public long getWorkaroundId() {
		return workaroundId;
	}

	/**
	 * Sets the '{@link WorkaroundNotes#getWorkaroundId() <em>workaroundId</em>}
	 * ' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newWorkaroundId
	 *            the new value of the '
	 *            {@link WorkaroundNotes#getWorkaroundId() workaroundId}'
	 *            feature.
	 * @generated
	 */
	public void setWorkaroundId(long newWorkaroundId) {
		workaroundId = newWorkaroundId;

	}

}
