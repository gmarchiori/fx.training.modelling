package com.finantix.problemandsolution.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.eclipse.persistence.annotations.Multitenant;
import org.eclipse.persistence.annotations.TenantDiscriminatorColumn;
import org.eclipse.persistence.annotations.TenantDiscriminatorColumns;
import org.eclipse.persistence.annotations.VirtualAccessMethods;
import org.eclipse.persistence.oxm.annotations.XmlVirtualAccessMethods;
import com.thedigitalstack.model.converter.EntityReferenceAdapter;

/**
 * A representation of the model object '<em><b>Solution</b></em>'.<br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */
@Entity(name = "problemandsolution_Solution")
@Multitenant()
@TenantDiscriminatorColumns({ @TenantDiscriminatorColumn(name = "tenant_id") })
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "code" }) })
@XmlVirtualAccessMethods()
@XmlRootElement(namespace = ProblemandsolutionModelPackage.NS_URI)
@VirtualAccessMethods()
public abstract class Solution extends com.thedigitalstack.model.Entity

{
	/**
	 * @generated
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Id()
	@GeneratedValue(generator = "problemandsolution_solution")
	@SequenceGenerator(name = "problemandsolution_solution", sequenceName = "problemandsolution_solution_seq")
	private long id = 0;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Basic()
	private String solutionType = null;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Basic()
	@Column(length = 16, name = "code")
	private String code = null;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Basic()
	private String description = null;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Basic()
	@Temporal(TemporalType.DATE)
	private Date deliveryDate = null;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Basic()
	private String releasedIn = null;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@ManyToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH })
	@JoinTable(name = "problemandsolution_Solution_problemRel")
	@OrderColumn()
	private List<Problem> problemRel = new ArrayList<Problem>();

	/**
	 * Identifier of the class.
	 * 
	 * @generated
	 */
	public static final String CLASS_ID = "solutions";

	/**
	 * Returns the value of '<em><b>id</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>id</b></em>' feature
	 * @generated
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the '{@link Solution#getId() <em>id</em>}' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newId
	 *            the new value of the '{@link Solution#getId() id}' feature.
	 * @generated
	 */
	public void setId(long newId) {
		id = newId;

	}

	/**
	 * Returns the value of '<em><b>solutionType</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>solutionType</b></em>' feature
	 * @generated
	 */
	@XmlJavaTypeAdapter(ProblemandsolutionModelPackage.RefSolutionTypeWeakAdapter.class)
	public String getSolutionType() {
		return solutionType;
	}

	/**
	 * Sets the '{@link Solution#getSolutionType() <em>solutionType</em>}'
	 * feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newSolutionType
	 *            the new value of the '{@link Solution#getSolutionType()
	 *            solutionType}' feature.
	 * @generated
	 */
	public void setSolutionType(String newSolutionType) {
		solutionType = newSolutionType;

	}

	/**
	 * Returns the value of '<em><b>code</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>code</b></em>' feature
	 * @generated
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the '{@link Solution#getCode() <em>code</em>}' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newCode
	 *            the new value of the '{@link Solution#getCode() code}'
	 *            feature.
	 * @generated
	 */
	public void setCode(String newCode) {
		code = newCode;

	}

	/**
	 * Returns the value of '<em><b>description</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>description</b></em>' feature
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the '{@link Solution#getDescription() <em>description</em>}'
	 * feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newDescription
	 *            the new value of the '{@link Solution#getDescription()
	 *            description}' feature.
	 * @generated
	 */
	public void setDescription(String newDescription) {
		description = newDescription;

	}

	/**
	 * Returns the value of '<em><b>deliveryDate</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>deliveryDate</b></em>' feature
	 * @generated
	 */
	public Date getDeliveryDate() {
		return deliveryDate;
	}

	/**
	 * Sets the '{@link Solution#getDeliveryDate() <em>deliveryDate</em>}'
	 * feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newDeliveryDate
	 *            the new value of the '{@link Solution#getDeliveryDate()
	 *            deliveryDate}' feature.
	 * @generated
	 */
	public void setDeliveryDate(Date newDeliveryDate) {
		deliveryDate = newDeliveryDate;

	}

	/**
	 * Returns the value of '<em><b>releasedIn</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>releasedIn</b></em>' feature
	 * @generated
	 */
	public String getReleasedIn() {
		return releasedIn;
	}

	/**
	 * Sets the '{@link Solution#getReleasedIn() <em>releasedIn</em>}' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newReleasedIn
	 *            the new value of the '{@link Solution#getReleasedIn()
	 *            releasedIn}' feature.
	 * @generated
	 */
	public void setReleasedIn(String newReleasedIn) {
		releasedIn = newReleasedIn;

	}

	/**
	 * Returns the value of '<em><b>problemRel</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>problemRel</b></em>' feature
	 * @generated
	 */
	@XmlJavaTypeAdapter(EntityReferenceAdapter.class)
	public List<Problem> getProblemRel() {
		return problemRel;
	}

	/**
	 * Adds to the <em>problemRel</em> feature.
	 * 
	 * @param problemRelValue
	 *            the value to add
	 * @generated
	 */
	public void addToProblemRel(Problem problemRelValue) {
		if (!problemRel.contains(problemRelValue)) {
			problemRel.add(problemRelValue);
		}
	}

	/**
	 * Removes from the <em>problemRel</em> feature.
	 * 
	 * @param problemRelValue
	 *            the value to delete
	 * @generated
	 */
	public void removeFromProblemRel(Problem problemRelValue) {
		if (problemRel.contains(problemRelValue)) {
			problemRel.remove(problemRelValue);
		}
	}

	/**
	 * Clears the <em>problemRel</em> feature.
	 * 
	 * @generated
	 */
	public void clearProblemRel() {
		while (!problemRel.isEmpty()) {
			removeFromProblemRel(problemRel.iterator().next());
		}
	}

	/**
	 * Sets the '{@link Solution#getProblemRel() <em>problemRel</em>}' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newProblemRel
	 *            the new value of the '{@link Solution#getProblemRel()
	 *            problemRel}' feature.
	 * @generated
	 */
	public void setProblemRel(List<Problem> newProblemRel) {
		problemRel = newProblemRel;

	}

}
