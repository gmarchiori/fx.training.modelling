package com.finantix.problemandsolution.core;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.persistence.annotations.Multitenant;
import org.eclipse.persistence.annotations.TenantDiscriminatorColumn;
import org.eclipse.persistence.annotations.TenantDiscriminatorColumns;
import org.eclipse.persistence.annotations.VirtualAccessMethods;
import org.eclipse.persistence.oxm.annotations.XmlVirtualAccessMethods;

/**
 * A representation of the model object '<em><b>Workaround</b></em>'.<br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */
@Entity(name = "problemandsolution_Workaround")
@Multitenant()
@TenantDiscriminatorColumns({ @TenantDiscriminatorColumn(name = "tenant_id") })
@XmlVirtualAccessMethods()
@XmlRootElement(namespace = ProblemandsolutionModelPackage.NS_URI)
@VirtualAccessMethods()
public class Workaround extends Solution

{
	/**
	 * @generated
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Basic()
	private boolean acceptedByClient = false;

	/**
	 * Identifier of the class.
	 * 
	 * @generated
	 */
	public static final String CLASS_ID = "workarounds";

	/**
	 * Returns the value of '<em><b>acceptedByClient</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>acceptedByClient</b></em>' feature
	 * @generated
	 */
	public boolean isAcceptedByClient() {
		return acceptedByClient;
	}

	/**
	 * Sets the '{@link Workaround#isAcceptedByClient()
	 * <em>acceptedByClient</em>}' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newAcceptedByClient
	 *            the new value of the '{@link Workaround#isAcceptedByClient()
	 *            acceptedByClient}' feature.
	 * @generated
	 */
	public void setAcceptedByClient(boolean newAcceptedByClient) {
		acceptedByClient = newAcceptedByClient;

	}

}
