package com.finantix.problemandsolution.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.texo.model.ModelFactory;
import org.eclipse.emf.texo.model.ModelPackage;
import org.eclipse.emf.texo.model.ModelResolver;
import com.finantix.problemandsolution.core.dao.FixDao;
import com.finantix.problemandsolution.core.dao.ProblemDao;
import com.finantix.problemandsolution.core.dao.RefSolutionTypeDao;
import com.finantix.problemandsolution.core.dao.WorkaroundDao;
import com.finantix.problemandsolution.core.dao.WorkaroundNotesDao;
import com.finantix.training.shared.core.SharedModelPackage;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.model.Models;
import com.thedigitalstack.model.binding.IContainerList;
import com.thedigitalstack.model.converter.AbstractWeakLinkAdapter;
import com.thedigitalstack.model.internal.metadata.DaoRegistry;
import com.thedigitalstack.model.metadata.IDaoRegistry;
import com.thedigitalstack.model.metadata.TenantModelPackage;

/**
 * The <b>Package</b> for the model '<em><b>problemandsolution</b></em>'. It
 * contains initialization code and access to the Factory to instantiate types
 * of this package.<br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */
public class ProblemandsolutionModelPackage extends TenantModelPackage {

	/**
	 * RefSolutionTypeWeakAdapter.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static class RefSolutionTypeWeakAdapter extends
			AbstractWeakLinkAdapter {

		/**
		 * Default constructor.<br>
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */

		public RefSolutionTypeWeakAdapter() {

			super(NS_URI, getInstance().getRefSolutionTypeEClass().getName());

		}

	};

	/**
	 * WorkaroundWeakAdapter.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static class WorkaroundWeakAdapter extends AbstractWeakLinkAdapter {

		/**
		 * Default constructor.<br>
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */

		public WorkaroundWeakAdapter() {

			super(NS_URI, getInstance().getWorkaroundEClass().getName());

		}

	};

	/**
	 * The package namespace URI.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final String NS_URI = "com.finantix.problemandsolution.core.model";

	/**
	 * The package name.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final String NAME = "problemandsolution";

	/**
	 * The classifier id for the '{@link SolutionType <em>SolutionType</em>}'
	 * class.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int SOLUTIONTYPE_CLASSIFIER_ID = 6;

	/**
	 * The classifier id for the '{@link Solution <em>Solution</em>}' class.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int SOLUTION_CLASSIFIER_ID = 0;

	/**
	 * The feature id for the '{@link Solution <em>id</em>}' attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int SOLUTION_ID_FEATURE_ID = 0;

	/**
	 * The feature id for the '{@link Solution <em>solutionType</em>}'
	 * attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int SOLUTION_SOLUTIONTYPE_FEATURE_ID = 1;

	/**
	 * The feature id for the '{@link Solution <em>code</em>}' attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int SOLUTION_CODE_FEATURE_ID = 2;

	/**
	 * The feature id for the '{@link Solution <em>description</em>}' attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int SOLUTION_DESCRIPTION_FEATURE_ID = 3;

	/**
	 * The feature id for the '{@link Solution <em>deliveryDate</em>}'
	 * attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int SOLUTION_DELIVERYDATE_FEATURE_ID = 4;

	/**
	 * The feature id for the '{@link Solution <em>releasedIn</em>}' attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int SOLUTION_RELEASEDIN_FEATURE_ID = 5;

	/**
	 * The feature id for the '{@link Solution <em>problemRel</em>}' attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int SOLUTION_PROBLEMREL_FEATURE_ID = 6;

	/**
	 * The classifier id for the '{@link Problem <em>Problem</em>}' class.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int PROBLEM_CLASSIFIER_ID = 1;

	/**
	 * The feature id for the '{@link Problem <em>id</em>}' attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int PROBLEM_ID_FEATURE_ID = 0;

	/**
	 * The feature id for the '{@link Problem <em>code</em>}' attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int PROBLEM_CODE_FEATURE_ID = 1;

	/**
	 * The feature id for the '{@link Problem <em>description</em>}' attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int PROBLEM_DESCRIPTION_FEATURE_ID = 2;

	/**
	 * The feature id for the '{@link Problem <em>lastUpdated</em>}' attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int PROBLEM_LASTUPDATED_FEATURE_ID = 3;

	/**
	 * The classifier id for the '{@link Workaround <em>Workaround</em>}' class.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int WORKAROUND_CLASSIFIER_ID = 2;

	/**
	 * The feature id for the '{@link Workaround <em>acceptedByClient</em>}'
	 * attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int WORKAROUND_ACCEPTEDBYCLIENT_FEATURE_ID = 7;

	/**
	 * The classifier id for the '{@link WorkaroundNotes
	 * <em>WorkaroundNotes</em>}' class.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int WORKAROUNDNOTES_CLASSIFIER_ID = 3;

	/**
	 * The feature id for the '{@link WorkaroundNotes <em>id</em>}' attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int WORKAROUNDNOTES_ID_FEATURE_ID = 0;

	/**
	 * The feature id for the '{@link WorkaroundNotes <em>workaroundId</em>}'
	 * attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int WORKAROUNDNOTES_WORKAROUNDID_FEATURE_ID = 1;

	/**
	 * The feature id for the '{@link WorkaroundNotes <em>title</em>}'
	 * attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int WORKAROUNDNOTES_TITLE_FEATURE_ID = 2;

	/**
	 * The feature id for the '{@link WorkaroundNotes <em>content</em>}'
	 * attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int WORKAROUNDNOTES_CONTENT_FEATURE_ID = 3;

	/**
	 * The classifier id for the '{@link Fix <em>Fix</em>}' class.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int FIX_CLASSIFIER_ID = 4;

	/**
	 * The feature id for the '{@link Fix <em>commitRef</em>}' attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int FIX_COMMITREF_FEATURE_ID = 7;

	/**
	 * The classifier id for the '{@link RefSolutionType
	 * <em>RefSolutionType</em>}' class.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int REFSOLUTIONTYPE_CLASSIFIER_ID = 5;

	/**
	 * The feature id for the '{@link RefSolutionType <em>solutionType</em>}'
	 * attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int REFSOLUTIONTYPE_SOLUTIONTYPE_FEATURE_ID = 5;

	/**
	 * The feature id for the '{@link RefSolutionType <em>key</em>}' attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int REFSOLUTIONTYPE_KEY_FEATURE_ID = 6;

	/**
	 * The feature id for the '{@link RefSolutionType <em>value</em>}'
	 * attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int REFSOLUTIONTYPE_VALUE_FEATURE_ID = 7;

	/**
	 * The feature id for the '{@link RefSolutionType <em>displayValue</em>}'
	 * attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int REFSOLUTIONTYPE_DISPLAYVALUE_FEATURE_ID = 0;

	/**
	 * The feature id for the '{@link RefSolutionType <em>displayOrder</em>}'
	 * attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int REFSOLUTIONTYPE_DISPLAYORDER_FEATURE_ID = 1;

	/**
	 * The feature id for the '{@link RefSolutionType
	 * <em>startValidationDate</em>}' attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int REFSOLUTIONTYPE_STARTVALIDATIONDATE_FEATURE_ID = 2;

	/**
	 * The feature id for the '{@link RefSolutionType
	 * <em>endValidationDate</em>}' attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int REFSOLUTIONTYPE_ENDVALIDATIONDATE_FEATURE_ID = 3;

	/**
	 * The feature id for the '{@link RefSolutionType <em>replacedBy</em>}'
	 * attribute.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final int REFSOLUTIONTYPE_REPLACEDBY_FEATURE_ID = 4;

	/**
	 * The {@link ModelFactory} for this package.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private ProblemandsolutionModelFactory factory;

	/**
	 * The {@com.thedigitalstack.model.metadata.IDaoRegistry} for this package.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static final IDaoRegistry daoRegistry = new DaoRegistry(NS_URI); // NOSONAR

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	static {

		daoRegistry.registerDao(Problem.class, ProblemDao.class);

		daoRegistry.registerDao(Workaround.class, WorkaroundDao.class);

		daoRegistry
				.registerDao(WorkaroundNotes.class, WorkaroundNotesDao.class);

		daoRegistry.registerDao(Fix.class, FixDao.class);

		daoRegistry
				.registerDao(RefSolutionType.class, RefSolutionTypeDao.class);

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ProblemandsolutionModelPackage() {// NOSONAR
	}

	/**
	 * Returns the {@link ProblemandsolutionModelFactory} of this ModelPackage.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the {@link ProblemandsolutionModelFactory} instance.
	 * @generated
	 */
	public ProblemandsolutionModelFactory getFactory() {
		return this.factory;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public IDaoRegistry getDaoRegistry() {
		return daoRegistry;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	@NonNull
	protected Class<TenantModelPackage>[] getLinkedModelPackageClasses() {
		List<Class<? extends TenantModelPackage>> list = new ArrayList<Class<? extends TenantModelPackage>>();

		list.add(SharedModelPackage.class);

		Class<TenantModelPackage>[] result = new Class[list.size()];
		list.toArray(result);
		return result;
	}

	/**
	 * Initializes this {@link ModelPackage}.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void doRegister(ResourceSet resourceSet, ModelResolver resolver) { // NOSONAR

		if (!registerModelPackageFromFile(resolver, resourceSet)) {
			return;
		}
		super.doRegister(resourceSet, resolver);
		factory = new ProblemandsolutionModelFactory(this);

		// read the model from the ecore file, the EPackage is registered in the
		// EPackage.Registry
		// see the ModelResolver getEPackageRegistry method
		resolver.registerModelPackage(this);

		// register the relation between a Class and its EClassifier

		resolver.registerClassModelMapping(Solution.class,
				this.getSolutionEClass(), this);

		resolver.registerClassModelMapping(Problem.class,
				this.getProblemEClass(), this);

		resolver.registerClassModelMapping(Workaround.class,
				this.getWorkaroundEClass(), this);

		resolver.registerClassModelMapping(WorkaroundNotes.class,
				this.getWorkaroundNotesEClass(), this);

		resolver.registerClassModelMapping(Fix.class, this.getFixEClass(), this);

		resolver.registerClassModelMapping(RefSolutionType.class,
				this.getRefSolutionTypeEClass(), this);

		resolver.registerClassModelMapping(SolutionType.class,
				this.getSolutionTypeEEnum(), this);

	}

	/**
	 * Returns the {@link ModelFactory} of this ModelPackage.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the {@link ProblemandsolutionModelFactory} instance.
	 * @generated
	 */
	@Override
	public ProblemandsolutionModelFactory getModelFactory() {
		return factory;
	}

	/**
	 * Returns the nsUri of the {@link EPackage} managed by this Package
	 * instance.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the nsUri of the EPackage
	 * @generated
	 */
	@Override
	public String getNsURI() {
		return NS_URI;
	}

	/**
	 * Returns the name of the ecore file containing the ecore model of the
	 * {@link EPackage} managed here.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the name of the ecore file
	 * @generated
	 */
	@Override
	public String getEcoreFileName() {
		return "problemandsolution.ecore";
	}

	/**
	 * Returns the {@link EClass} '<em><b>Solution</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EClass} '<em><b>Solution</b></em>'
	 * @generated
	 */
	public EClass getSolutionEClass() {
		return (EClass) getEPackage().getEClassifiers().get(
				SOLUTION_CLASSIFIER_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '<em><b>Solution.id</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>Solution.id</b></em>'.
	 * @generated
	 */

	public EAttribute getSolution_Id() {
		return (EAttribute) getSolutionEClass().getEAllStructuralFeatures()
				.get(SOLUTION_ID_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>Solution.solutionType</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>Solution.solutionType</b></em>'.
	 * @generated
	 */

	public EAttribute getSolution_SolutionType() {
		return (EAttribute) getSolutionEClass().getEAllStructuralFeatures()
				.get(SOLUTION_SOLUTIONTYPE_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '<em><b>Solution.code</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>Solution.code</b></em>'.
	 * @generated
	 */

	public EAttribute getSolution_Code() {
		return (EAttribute) getSolutionEClass().getEAllStructuralFeatures()
				.get(SOLUTION_CODE_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>Solution.description</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>Solution.description</b></em>'.
	 * @generated
	 */

	public EAttribute getSolution_Description() {
		return (EAttribute) getSolutionEClass().getEAllStructuralFeatures()
				.get(SOLUTION_DESCRIPTION_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>Solution.deliveryDate</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>Solution.deliveryDate</b></em>'.
	 * @generated
	 */

	public EAttribute getSolution_DeliveryDate() {
		return (EAttribute) getSolutionEClass().getEAllStructuralFeatures()
				.get(SOLUTION_DELIVERYDATE_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>Solution.releasedIn</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>Solution.releasedIn</b></em>'.
	 * @generated
	 */

	public EAttribute getSolution_ReleasedIn() {
		return (EAttribute) getSolutionEClass().getEAllStructuralFeatures()
				.get(SOLUTION_RELEASEDIN_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>Solution.problemRel</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>Solution.problemRel</b></em>'.
	 * @generated
	 */

	public EReference getSolution_ProblemRel() {
		return (EReference) getSolutionEClass().getEAllStructuralFeatures()
				.get(SOLUTION_PROBLEMREL_FEATURE_ID);
	}

	/**
	 * Returns the {@link EClass} '<em><b>Problem</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EClass} '<em><b>Problem</b></em>'
	 * @generated
	 */
	public EClass getProblemEClass() {
		return (EClass) getEPackage().getEClassifiers().get(
				PROBLEM_CLASSIFIER_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '<em><b>Problem.id</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>Problem.id</b></em>'.
	 * @generated
	 */

	public EAttribute getProblem_Id() {
		return (EAttribute) getProblemEClass().getEAllStructuralFeatures().get(
				PROBLEM_ID_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '<em><b>Problem.code</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>Problem.code</b></em>'.
	 * @generated
	 */

	public EAttribute getProblem_Code() {
		return (EAttribute) getProblemEClass().getEAllStructuralFeatures().get(
				PROBLEM_CODE_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>Problem.description</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>Problem.description</b></em>'.
	 * @generated
	 */

	public EAttribute getProblem_Description() {
		return (EAttribute) getProblemEClass().getEAllStructuralFeatures().get(
				PROBLEM_DESCRIPTION_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>Problem.lastUpdated</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>Problem.lastUpdated</b></em>'.
	 * @generated
	 */

	public EAttribute getProblem_LastUpdated() {
		return (EAttribute) getProblemEClass().getEAllStructuralFeatures().get(
				PROBLEM_LASTUPDATED_FEATURE_ID);
	}

	/**
	 * Returns the {@link EClass} '<em><b>Workaround</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EClass} '<em><b>Workaround</b></em>'
	 * @generated
	 */
	public EClass getWorkaroundEClass() {
		return (EClass) getEPackage().getEClassifiers().get(
				WORKAROUND_CLASSIFIER_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>Workaround.acceptedByClient</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>Workaround.acceptedByClient</b></em>'.
	 * @generated
	 */

	public EAttribute getWorkaround_AcceptedByClient() {
		return (EAttribute) getWorkaroundEClass().getEAllStructuralFeatures()
				.get(WORKAROUND_ACCEPTEDBYCLIENT_FEATURE_ID);
	}

	/**
	 * Returns the {@link EClass} '<em><b>WorkaroundNotes</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EClass} '
	 *         <em><b>WorkaroundNotes</b></em>'
	 * @generated
	 */
	public EClass getWorkaroundNotesEClass() {
		return (EClass) getEPackage().getEClassifiers().get(
				WORKAROUNDNOTES_CLASSIFIER_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>WorkaroundNotes.id</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>WorkaroundNotes.id</b></em>'.
	 * @generated
	 */

	public EAttribute getWorkaroundNotes_Id() {
		return (EAttribute) getWorkaroundNotesEClass()
				.getEAllStructuralFeatures().get(WORKAROUNDNOTES_ID_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>WorkaroundNotes.workaroundId</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>WorkaroundNotes.workaroundId</b></em>'.
	 * @generated
	 */

	public EAttribute getWorkaroundNotes_WorkaroundId() {
		return (EAttribute) getWorkaroundNotesEClass()
				.getEAllStructuralFeatures().get(
						WORKAROUNDNOTES_WORKAROUNDID_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>WorkaroundNotes.title</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>WorkaroundNotes.title</b></em>'.
	 * @generated
	 */

	public EAttribute getWorkaroundNotes_Title() {
		return (EAttribute) getWorkaroundNotesEClass()
				.getEAllStructuralFeatures().get(
						WORKAROUNDNOTES_TITLE_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>WorkaroundNotes.content</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>WorkaroundNotes.content</b></em>'.
	 * @generated
	 */

	public EAttribute getWorkaroundNotes_Content() {
		return (EAttribute) getWorkaroundNotesEClass()
				.getEAllStructuralFeatures().get(
						WORKAROUNDNOTES_CONTENT_FEATURE_ID);
	}

	/**
	 * Returns the {@link EClass} '<em><b>Fix</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EClass} '<em><b>Fix</b></em>'
	 * @generated
	 */
	public EClass getFixEClass() {
		return (EClass) getEPackage().getEClassifiers().get(FIX_CLASSIFIER_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '<em><b>Fix.commitRef</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>Fix.commitRef</b></em>'.
	 * @generated
	 */

	public EAttribute getFix_CommitRef() {
		return (EAttribute) getFixEClass().getEAllStructuralFeatures().get(
				FIX_COMMITREF_FEATURE_ID);
	}

	/**
	 * Returns the {@link EClass} '<em><b>RefSolutionType</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EClass} '
	 *         <em><b>RefSolutionType</b></em>'
	 * @generated
	 */
	public EClass getRefSolutionTypeEClass() {
		return (EClass) getEPackage().getEClassifiers().get(
				REFSOLUTIONTYPE_CLASSIFIER_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>RefSolutionType.solutionType</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>RefSolutionType.solutionType</b></em>'.
	 * @generated
	 */

	public EAttribute getRefSolutionType_SolutionType() {
		return (EAttribute) getRefSolutionTypeEClass()
				.getEAllStructuralFeatures().get(
						REFSOLUTIONTYPE_SOLUTIONTYPE_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>RefSolutionType.key</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>RefSolutionType.key</b></em>'.
	 * @generated
	 */

	public EAttribute getRefSolutionType_Key() {
		return (EAttribute) getRefSolutionTypeEClass()
				.getEAllStructuralFeatures()
				.get(REFSOLUTIONTYPE_KEY_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>RefSolutionType.value</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>RefSolutionType.value</b></em>'.
	 * @generated
	 */

	public EAttribute getRefSolutionType_Value() {
		return (EAttribute) getRefSolutionTypeEClass()
				.getEAllStructuralFeatures().get(
						REFSOLUTIONTYPE_VALUE_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>RefSolutionType.displayValue</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>RefSolutionType.displayValue</b></em>'.
	 * @generated
	 */

	public EAttribute getRefSolutionType_DisplayValue() {
		return (EAttribute) getRefSolutionTypeEClass()
				.getEAllStructuralFeatures().get(
						REFSOLUTIONTYPE_DISPLAYVALUE_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>RefSolutionType.displayOrder</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>RefSolutionType.displayOrder</b></em>'.
	 * @generated
	 */

	public EAttribute getRefSolutionType_DisplayOrder() {
		return (EAttribute) getRefSolutionTypeEClass()
				.getEAllStructuralFeatures().get(
						REFSOLUTIONTYPE_DISPLAYORDER_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>RefSolutionType.startValidationDate</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>RefSolutionType.startValidationDate</b></em>'.
	 * @generated
	 */

	public EAttribute getRefSolutionType_StartValidationDate() {
		return (EAttribute) getRefSolutionTypeEClass()
				.getEAllStructuralFeatures().get(
						REFSOLUTIONTYPE_STARTVALIDATIONDATE_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>RefSolutionType.endValidationDate</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>RefSolutionType.endValidationDate</b></em>'.
	 * @generated
	 */

	public EAttribute getRefSolutionType_EndValidationDate() {
		return (EAttribute) getRefSolutionTypeEClass()
				.getEAllStructuralFeatures().get(
						REFSOLUTIONTYPE_ENDVALIDATIONDATE_FEATURE_ID);
	}

	/**
	 * Returns the {@link EStructuralFeature} '
	 * <em><b>RefSolutionType.replacedBy</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the {@link EStructuralFeature}: '
	 *         <em><b>RefSolutionType.replacedBy</b></em>'.
	 * @generated
	 */

	public EAttribute getRefSolutionType_ReplacedBy() {
		return (EAttribute) getRefSolutionTypeEClass()
				.getEAllStructuralFeatures().get(
						REFSOLUTIONTYPE_REPLACEDBY_FEATURE_ID);
	}

	/**
	 * Returns the EEnum '<em><b>SolutionType</b></em>'.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return an instance of the EEnum representing '
	 *         <em><b>SolutionType</b></em>'
	 * @generated
	 */
	public EEnum getSolutionTypeEEnum() {
		return (EEnum) getEPackage().getEClassifiers().get(
				SOLUTIONTYPE_CLASSIFIER_ID);
	}

	/**
	 * Returns the class implementing a specific {@link EClass}.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param eClassifier
	 *            the {@link EClassifier}
	 * @return the class implementing a specific {@link EClass}.
	 * @generated
	 */
	@Override
	public Class<?> getEClassifierClass(EClassifier eClassifier) {
		switch (eClassifier.getClassifierID()) {

		case SOLUTION_CLASSIFIER_ID:
			return Solution.class;

		case PROBLEM_CLASSIFIER_ID:
			return Problem.class;

		case WORKAROUND_CLASSIFIER_ID:
			return Workaround.class;

		case WORKAROUNDNOTES_CLASSIFIER_ID:
			return WorkaroundNotes.class;

		case FIX_CLASSIFIER_ID:
			return Fix.class;

		case REFSOLUTIONTYPE_CLASSIFIER_ID:
			return RefSolutionType.class;

		case SOLUTIONTYPE_CLASSIFIER_ID:
			return SolutionType.class;

		}
		throw new IllegalArgumentException("The EClassifier '" + eClassifier
				+ "' is not defined in this EPackage");
	}

	/**
	 * The adapter/wrapper for the EClass '<em><b>Problem</b></em>'.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */

	@XmlRootElement(namespace = ProblemandsolutionModelPackage.NS_URI, name = "problems")
	public static class ProblemXMLContainer implements IContainerList<Problem> {

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		private List<Problem> items;

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public ProblemXMLContainer() { // NOSONAR
			this.items = new ArrayList<Problem>(0);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public ProblemXMLContainer(List<Problem> items) { // NOSONAR
			this.items = items;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public ProblemXMLContainer(Problem item) { // NOSONAR
			this.items = Collections.singletonList(item);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@XmlElement(name = "problem")
		public List<Problem> getList() { // NOSONAR
			return items;
		}

	}

	/**
	 * The adapter/wrapper for the EClass '<em><b>Workaround</b></em>'.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */

	@XmlRootElement(namespace = ProblemandsolutionModelPackage.NS_URI, name = "workarounds")
	public static class WorkaroundXMLContainer implements
			IContainerList<Workaround> {

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		private List<Workaround> items;

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public WorkaroundXMLContainer() { // NOSONAR
			this.items = new ArrayList<Workaround>(0);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public WorkaroundXMLContainer(List<Workaround> items) { // NOSONAR
			this.items = items;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public WorkaroundXMLContainer(Workaround item) { // NOSONAR
			this.items = Collections.singletonList(item);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@XmlElement(name = "workaround")
		public List<Workaround> getList() { // NOSONAR
			return items;
		}

	}

	/**
	 * The adapter/wrapper for the EClass '<em><b>WorkaroundNotes</b></em>'.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */

	@XmlRootElement(namespace = ProblemandsolutionModelPackage.NS_URI, name = "workaroundNotess")
	public static class WorkaroundNotesXMLContainer implements
			IContainerList<WorkaroundNotes> {

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		private List<WorkaroundNotes> items;

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public WorkaroundNotesXMLContainer() { // NOSONAR
			this.items = new ArrayList<WorkaroundNotes>(0);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public WorkaroundNotesXMLContainer(List<WorkaroundNotes> items) { // NOSONAR
			this.items = items;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public WorkaroundNotesXMLContainer(WorkaroundNotes item) { // NOSONAR
			this.items = Collections.singletonList(item);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@XmlElement(name = "workaroundNotes")
		public List<WorkaroundNotes> getList() { // NOSONAR
			return items;
		}

	}

	/**
	 * The adapter/wrapper for the EClass '<em><b>Fix</b></em>'.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */

	@XmlRootElement(namespace = ProblemandsolutionModelPackage.NS_URI, name = "fixs")
	public static class FixXMLContainer implements IContainerList<Fix> {

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		private List<Fix> items;

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public FixXMLContainer() { // NOSONAR
			this.items = new ArrayList<Fix>(0);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public FixXMLContainer(List<Fix> items) { // NOSONAR
			this.items = items;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public FixXMLContainer(Fix item) { // NOSONAR
			this.items = Collections.singletonList(item);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@XmlElement(name = "fix")
		public List<Fix> getList() { // NOSONAR
			return items;
		}

	}

	/**
	 * The adapter/wrapper for the EClass '<em><b>RefSolutionType</b></em>'.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */

	@XmlRootElement(namespace = ProblemandsolutionModelPackage.NS_URI, name = "refSolutionTypes")
	public static class RefSolutionTypeXMLContainer implements
			IContainerList<RefSolutionType> {

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		private List<RefSolutionType> items;

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public RefSolutionTypeXMLContainer() { // NOSONAR
			this.items = new ArrayList<RefSolutionType>(0);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public RefSolutionTypeXMLContainer(List<RefSolutionType> items) { // NOSONAR
			this.items = items;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public RefSolutionTypeXMLContainer(RefSolutionType item) { // NOSONAR
			this.items = Collections.singletonList(item);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@XmlElement(name = "refSolutionType")
		public List<RefSolutionType> getList() { // NOSONAR
			return items;
		}

	}

	/**
	 * Returns the class container of the list. <br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the class container of the list {@link EClass}.
	 * @generated
	 */
	public Collection<Class<?>> getContainers() {
		Collection<Class<?>> containers = new ArrayList<Class<?>>();

		containers.add(ProblemXMLContainer.class);

		containers.add(WorkaroundXMLContainer.class);

		containers.add(WorkaroundNotesXMLContainer.class);

		containers.add(FixXMLContainer.class);

		containers.add(RefSolutionTypeXMLContainer.class);

		return containers;
	}

	/**
	 * Creates a container for a specific {@link EClass}.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param eClass
	 *            creates a Object instance for this EClass
	 * @return an object that representing the container of the EClass instances
	 * @generated
	 */
	public Class<?> getContainer(EClass eClass) {
		switch (eClass.getClassifierID()) {

		case ProblemandsolutionModelPackage.PROBLEM_CLASSIFIER_ID:
			return ProblemXMLContainer.class;

		case ProblemandsolutionModelPackage.WORKAROUND_CLASSIFIER_ID:
			return WorkaroundXMLContainer.class;

		case ProblemandsolutionModelPackage.WORKAROUNDNOTES_CLASSIFIER_ID:
			return WorkaroundNotesXMLContainer.class;

		case ProblemandsolutionModelPackage.FIX_CLASSIFIER_ID:
			return FixXMLContainer.class;

		case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_CLASSIFIER_ID:
			return RefSolutionTypeXMLContainer.class;
		}
		throw new IllegalArgumentException("The EClass '" + eClass.getName()
				+ "' is not a valid EClass for this EPackage");
	}

	/**
	 * Returns the instance of the model Package.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the instance of the model Package
	 * @generated
	 */
	public static ProblemandsolutionModelPackage getInstance() {
		ProblemandsolutionModelPackage p = (ProblemandsolutionModelPackage) Models
				.getMetadataManager().getModelPackage(NS_URI);
		assert p != null;
		return p;
	}

}
