package com.finantix.problemandsolution.core;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlRootElement;
import org.eclipse.persistence.annotations.Multitenant;
import org.eclipse.persistence.annotations.TenantDiscriminatorColumn;
import org.eclipse.persistence.annotations.TenantDiscriminatorColumns;
import org.eclipse.persistence.annotations.VirtualAccessMethods;
import org.eclipse.persistence.oxm.annotations.XmlVirtualAccessMethods;

/**
 * A representation of the model object '<em><b>Fix</b></em>'.<br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */
@Entity(name = "problemandsolution_Fix")
@Multitenant()
@TenantDiscriminatorColumns({ @TenantDiscriminatorColumn(name = "tenant_id") })
@XmlVirtualAccessMethods()
@XmlRootElement(namespace = ProblemandsolutionModelPackage.NS_URI)
@VirtualAccessMethods()
public class Fix extends Solution

{
	/**
	 * @generated
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Basic()
	@Column(length = 40)
	private String commitRef = null;

	/**
	 * Identifier of the class.
	 * 
	 * @generated
	 */
	public static final String CLASS_ID = "fixs";

	/**
	 * Returns the value of '<em><b>commitRef</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>commitRef</b></em>' feature
	 * @generated
	 */
	public String getCommitRef() {
		return commitRef;
	}

	/**
	 * Sets the '{@link Fix#getCommitRef() <em>commitRef</em>}' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newCommitRef
	 *            the new value of the '{@link Fix#getCommitRef() commitRef}'
	 *            feature.
	 * @generated
	 */
	public void setCommitRef(String newCommitRef) {
		commitRef = newCommitRef;

	}

}
