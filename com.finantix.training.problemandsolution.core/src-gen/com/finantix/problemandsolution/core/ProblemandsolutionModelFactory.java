package com.finantix.problemandsolution.core;

import java.util.Date;
import java.util.List;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.texo.model.AbstractModelObject;
import org.eclipse.emf.texo.model.ModelFactory;
import org.eclipse.emf.texo.model.ModelFeatureMapEntry;
import org.eclipse.emf.texo.model.ModelObject;
import org.eclipse.emf.texo.model.ModelPackage;
import com.thedigitalstack.model.internal.metadata.TenantModelFactory;

/**
 * The <b>{@link ModelFactory}</b> for the types of this model:
 * problemandsolution. It contains code to create instances of
 * {@link ModelObject} wrappers and instances of EClasses and convert objects
 * back and forth from their String (XML) representation.<br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */
public class ProblemandsolutionModelFactory extends TenantModelFactory
		implements ModelFactory {
	/**
	 * @generated
	 */
	private ProblemandsolutionModelPackage modelPackage;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public ProblemandsolutionModelFactory(
			ProblemandsolutionModelPackage modelPackage) { // NOSONAR
		this.modelPackage = modelPackage;
	}

	/**
	 * Creates an instance for an {@link EClass}.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param eClass
	 *            creates a Object instance for this EClass
	 * @return an object representing the eClass
	 * @generated
	 */
	public Object create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case ProblemandsolutionModelPackage.PROBLEM_CLASSIFIER_ID:
			return createProblem();
		case ProblemandsolutionModelPackage.WORKAROUND_CLASSIFIER_ID:
			return createWorkaround();
		case ProblemandsolutionModelPackage.WORKAROUNDNOTES_CLASSIFIER_ID:
			return createWorkaroundNotes();
		case ProblemandsolutionModelPackage.FIX_CLASSIFIER_ID:
			return createFix();
		case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_CLASSIFIER_ID:
			return createRefSolutionType();
		}
		throw new IllegalArgumentException("The EClass '" + eClass.getName()
				+ "' is not a valid EClass for this EPackage");
	}

	/**
	 * Creates an instance for an {@link EClass}.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param eClass
	 *            the EClass of the object
	 * @param values
	 *            the list of the object
	 * @return an object container of the list
	 * @generated
	 */
	public Object createContainer(EClass eClass, List<?> values) {
		switch (eClass.getClassifierID()) {

		case ProblemandsolutionModelPackage.PROBLEM_CLASSIFIER_ID:
			return new ProblemandsolutionModelPackage.ProblemXMLContainer(
					(List<Problem>) values);

		case ProblemandsolutionModelPackage.WORKAROUND_CLASSIFIER_ID:
			return new ProblemandsolutionModelPackage.WorkaroundXMLContainer(
					(List<Workaround>) values);

		case ProblemandsolutionModelPackage.WORKAROUNDNOTES_CLASSIFIER_ID:
			return new ProblemandsolutionModelPackage.WorkaroundNotesXMLContainer(
					(List<WorkaroundNotes>) values);

		case ProblemandsolutionModelPackage.FIX_CLASSIFIER_ID:
			return new ProblemandsolutionModelPackage.FixXMLContainer(
					(List<Fix>) values);

		case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_CLASSIFIER_ID:
			return new ProblemandsolutionModelPackage.RefSolutionTypeXMLContainer(
					(List<RefSolutionType>) values);
		}
		throw new IllegalArgumentException("The EClass '" + eClass.getName()
				+ "' is not a valid EClass for this EPackage");
	}

	/**
	 * Wraps an object in a {@link ModelObject}.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param eClass
	 *            the EClass of the object
	 * @param adaptee
	 *            the object being wrapped/adapted
	 * @return the wrapper {@link ModelObject}
	 * @generated
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ModelObject createModelObject(EClass eClass, Object adaptee) {
		ModelObject<Object> modelObject = null;
		switch (eClass.getClassifierID()) {

		case ProblemandsolutionModelPackage.PROBLEM_CLASSIFIER_ID:
			modelObject = new ProblemModelObject(modelPackage);
			break;

		case ProblemandsolutionModelPackage.WORKAROUND_CLASSIFIER_ID:
			modelObject = new WorkaroundModelObject(modelPackage);
			break;

		case ProblemandsolutionModelPackage.WORKAROUNDNOTES_CLASSIFIER_ID:
			modelObject = new WorkaroundNotesModelObject(modelPackage);
			break;

		case ProblemandsolutionModelPackage.FIX_CLASSIFIER_ID:
			modelObject = new FixModelObject(modelPackage);
			break;

		case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_CLASSIFIER_ID:
			modelObject = new RefSolutionTypeModelObject(modelPackage);
			break;
		default:
			throw new IllegalArgumentException("The EClass '" + eClass
					+ "' is not defined in this EPackage");
		}
		modelObject.setTarget(adaptee);
		return modelObject;
	}

	/**
	 * Creates a feature map entry instance for a certain EStructuralFeature.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param eFeature
	 *            the feature map feature
	 * @return the pojo feature map entry
	 * @generated
	 */
	public Object createFeatureMapEntry(EStructuralFeature eFeature) {
		throw new IllegalArgumentException("The EStructuralFeature '"
				+ eFeature + "' is not a valid feature map in this EPackage");
	}

	/**
	 * Wraps a feature map entry pojo in a {@link AbstractModelFeatureMapEntry}.
	 * If the feature map entry is null then a new one is created.<br>
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param eFeature
	 *            the feature map feature of the object
	 * @param adaptee
	 *            the pojo feature map entry being wrapped/adapted
	 * @return the wrapper {@link ModelFeatureMapEntry}
	 * @generated
	 */
	public ModelFeatureMapEntry<?> createModelFeatureMapEntry(
			EStructuralFeature eFeature, Object adaptee) {
		throw new IllegalArgumentException("The EStructuralFeature '"
				+ eFeature + "' is not a valid feature map in this EPackage");
	}

	/**
	 * Create an instance of the model object representing the EClass Problem.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return an instance of the model object representing the EClass Problem
	 * @generated
	 */
	public Problem createProblem() {
		return new Problem();
	}

	/**
	 * Create an instance of the model object representing the EClass
	 * Workaround.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return an instance of the model object representing the EClass
	 *         Workaround
	 * @generated
	 */
	public Workaround createWorkaround() {
		return new Workaround();
	}

	/**
	 * Create an instance of the model object representing the EClass
	 * WorkaroundNotes.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return an instance of the model object representing the EClass
	 *         WorkaroundNotes
	 * @generated
	 */
	public WorkaroundNotes createWorkaroundNotes() {
		return new WorkaroundNotes();
	}

	/**
	 * Create an instance of the model object representing the EClass Fix.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return an instance of the model object representing the EClass Fix
	 * @generated
	 */
	public Fix createFix() {
		return new Fix();
	}

	/**
	 * Create an instance of the model object representing the EClass
	 * RefSolutionType.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return an instance of the model object representing the EClass
	 *         RefSolutionType
	 * @generated
	 */
	public RefSolutionType createRefSolutionType() {
		return new RefSolutionType();
	}

	/**
	 * Converts an instance of an {@link EDataType} to a String.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param eDataType
	 *            the {@link EDataType} defining the type
	 * @param value
	 *            the object to convert, if the value is null then null is
	 *            returned.
	 * @return the value converted
	 * @generated
	 */
	public Object createFromString(EDataType eDataType, String value) {

		switch (eDataType.getClassifierID()) {
		case ProblemandsolutionModelPackage.SOLUTIONTYPE_CLASSIFIER_ID:
			return createSolutionTypeFromString(value);
		}

		throw new IllegalArgumentException("The EDatatype '" + eDataType
				+ "' is not defined in this EPackage");
	}

	/**
	 * Converts an instance of an {@link EDataType} to a String.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param eDataType
	 *            the {@link EDataType} defining the type
	 * @param value
	 *            the object to convert, if value == null then null is returned
	 * @return the value converted
	 * @generated
	 */
	public String convertToString(EDataType eDataType, Object value) {

		switch (eDataType.getClassifierID()) {
		case ProblemandsolutionModelPackage.SOLUTIONTYPE_CLASSIFIER_ID:
			return convertSolutionTypeToString((SolutionType) value);
		}

		throw new IllegalArgumentException("The EDatatype '" + eDataType
				+ "' is not defined in this EPackage.");
	}

	/**
	 * Converts the EDataType: SolutionType to a String.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param value
	 *            the object to convert
	 * @return the String representing the value, if value == null then null is
	 *         returned
	 * @generated
	 */
	public String convertSolutionTypeToString(SolutionType value) {
		if (value == null) {
			return null;
		}
		return value.toString();
	}

	/**
	 * Creates an instance of the EDataType: SolutionType from a String.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param value
	 *            the string value to convert to an object
	 * @return the instance of the data type, if value == null then null is
	 *         returned
	 * @generated
	 */
	public SolutionType createSolutionTypeFromString(String value) {
		if (value == null) {
			return null;
		}
		return SolutionType.get(value);
	}

	/**
	 * The adapter/wrapper for the EClass '<em><b>Solution</b></em>'.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static class SolutionModelObject<E extends Solution>

	extends AbstractModelObject<E>

	{

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		private ProblemandsolutionModelPackage modelPackage;

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public SolutionModelObject(ProblemandsolutionModelPackage modelPackage) { // NOSONAR

			this.modelPackage = modelPackage;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public EClass eClass() {
			return modelPackage.getSolutionEClass();
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public ModelPackage getModelPackage() {
			return modelPackage;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public Object eGet(EStructuralFeature eStructuralFeature) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			case ProblemandsolutionModelPackage.SOLUTION_ID_FEATURE_ID:
				return getTarget().getId();

			case ProblemandsolutionModelPackage.SOLUTION_SOLUTIONTYPE_FEATURE_ID:
				return getTarget().getSolutionType();

			case ProblemandsolutionModelPackage.SOLUTION_CODE_FEATURE_ID:
				return getTarget().getCode();

			case ProblemandsolutionModelPackage.SOLUTION_DESCRIPTION_FEATURE_ID:
				return getTarget().getDescription();

			case ProblemandsolutionModelPackage.SOLUTION_DELIVERYDATE_FEATURE_ID:
				return getTarget().getDeliveryDate();

			case ProblemandsolutionModelPackage.SOLUTION_RELEASEDIN_FEATURE_ID:
				return getTarget().getReleasedIn();

			case ProblemandsolutionModelPackage.SOLUTION_PROBLEMREL_FEATURE_ID:
				return getTarget().getProblemRel();

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eContainer())) {
					return getTarget().get(eStructuralFeature.getName());
				}

			}

			return super.eGet(eStructuralFeature);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */

		@Override
		public void eSet(EStructuralFeature eStructuralFeature, Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			case ProblemandsolutionModelPackage.SOLUTION_ID_FEATURE_ID:
				getTarget().setId((Long) value);
				return;

			case ProblemandsolutionModelPackage.SOLUTION_SOLUTIONTYPE_FEATURE_ID:
				getTarget().setSolutionType((String) value);
				return;

			case ProblemandsolutionModelPackage.SOLUTION_CODE_FEATURE_ID:
				getTarget().setCode((String) value);
				return;

			case ProblemandsolutionModelPackage.SOLUTION_DESCRIPTION_FEATURE_ID:
				getTarget().setDescription((String) value);
				return;

			case ProblemandsolutionModelPackage.SOLUTION_DELIVERYDATE_FEATURE_ID:
				getTarget().setDeliveryDate((Date) value);
				return;

			case ProblemandsolutionModelPackage.SOLUTION_RELEASEDIN_FEATURE_ID:
				getTarget().setReleasedIn((String) value);
				return;

			case ProblemandsolutionModelPackage.SOLUTION_PROBLEMREL_FEATURE_ID:
				getTarget().setProblemRel((List<Problem>) value);
				return;

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eContainer())) {
					getTarget().set(eStructuralFeature.getName(), value);
					return;
				}

			}

			super.eSet(eStructuralFeature, value);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public void eAddTo(EStructuralFeature eStructuralFeature, Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			case ProblemandsolutionModelPackage.SOLUTION_PROBLEMREL_FEATURE_ID:
				getTarget().getProblemRel().add((Problem) value);
				return;

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eClass())) {
					@SuppressWarnings("rawtypes")
					List l = (List) getTarget().get(
							eStructuralFeature.getName());
					if (l != null) {
						l.add(value);
					}
					return;
				}

			}

			super.eAddTo(eStructuralFeature, value);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public void eRemoveFrom(EStructuralFeature eStructuralFeature,
				Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			case ProblemandsolutionModelPackage.SOLUTION_PROBLEMREL_FEATURE_ID:
				getTarget().getProblemRel().remove(value);
				return;

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eClass())) {
					@SuppressWarnings("rawtypes")
					List l = (List) getTarget().get(
							eStructuralFeature.getName());
					if (l != null) {
						l.remove(value);
					}
					return;
				}

			}

			super.eRemoveFrom(eStructuralFeature, value);
		}
	}

	/**
	 * The adapter/wrapper for the EClass '<em><b>Problem</b></em>'.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static class ProblemModelObject<E extends Problem>

	extends AbstractModelObject<E>

	{

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		private ProblemandsolutionModelPackage modelPackage;

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public ProblemModelObject(ProblemandsolutionModelPackage modelPackage) { // NOSONAR

			this.modelPackage = modelPackage;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public EClass eClass() {
			return modelPackage.getProblemEClass();
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public ModelPackage getModelPackage() {
			return modelPackage;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public Object eGet(EStructuralFeature eStructuralFeature) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			case ProblemandsolutionModelPackage.PROBLEM_ID_FEATURE_ID:
				return getTarget().getId();

			case ProblemandsolutionModelPackage.PROBLEM_CODE_FEATURE_ID:
				return getTarget().getCode();

			case ProblemandsolutionModelPackage.PROBLEM_DESCRIPTION_FEATURE_ID:
				return getTarget().getDescription();

			case ProblemandsolutionModelPackage.PROBLEM_LASTUPDATED_FEATURE_ID:
				return getTarget().getLastUpdated();

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eContainer())) {
					return getTarget().get(eStructuralFeature.getName());
				}

			}

			return super.eGet(eStructuralFeature);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */

		@Override
		public void eSet(EStructuralFeature eStructuralFeature, Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			case ProblemandsolutionModelPackage.PROBLEM_ID_FEATURE_ID:
				getTarget().setId((Long) value);
				return;

			case ProblemandsolutionModelPackage.PROBLEM_CODE_FEATURE_ID:
				getTarget().setCode((String) value);
				return;

			case ProblemandsolutionModelPackage.PROBLEM_DESCRIPTION_FEATURE_ID:
				getTarget().setDescription((String) value);
				return;

			case ProblemandsolutionModelPackage.PROBLEM_LASTUPDATED_FEATURE_ID:
				getTarget().setLastUpdated((Date) value);
				return;

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eContainer())) {
					getTarget().set(eStructuralFeature.getName(), value);
					return;
				}

			}

			super.eSet(eStructuralFeature, value);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public void eAddTo(EStructuralFeature eStructuralFeature, Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eClass())) {
					@SuppressWarnings("rawtypes")
					List l = (List) getTarget().get(
							eStructuralFeature.getName());
					if (l != null) {
						l.add(value);
					}
					return;
				}

			}

			super.eAddTo(eStructuralFeature, value);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public void eRemoveFrom(EStructuralFeature eStructuralFeature,
				Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eClass())) {
					@SuppressWarnings("rawtypes")
					List l = (List) getTarget().get(
							eStructuralFeature.getName());
					if (l != null) {
						l.remove(value);
					}
					return;
				}

			}

			super.eRemoveFrom(eStructuralFeature, value);
		}
	}

	/**
	 * The adapter/wrapper for the EClass '<em><b>Workaround</b></em>'.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static class WorkaroundModelObject<E extends Workaround>

	extends SolutionModelObject<E>

	{

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		private ProblemandsolutionModelPackage modelPackage;

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public WorkaroundModelObject(ProblemandsolutionModelPackage modelPackage) { // NOSONAR

			super((ProblemandsolutionModelPackage) modelPackage
					.getModelPackage(ProblemandsolutionModelPackage.class));

			this.modelPackage = modelPackage;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public EClass eClass() {
			return modelPackage.getWorkaroundEClass();
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public ModelPackage getModelPackage() {
			return modelPackage;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public Object eGet(EStructuralFeature eStructuralFeature) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			case ProblemandsolutionModelPackage.WORKAROUND_ACCEPTEDBYCLIENT_FEATURE_ID:
				return getTarget().isAcceptedByClient();

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eContainer())) {
					return getTarget().get(eStructuralFeature.getName());
				}

			}

			return super.eGet(eStructuralFeature);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */

		@Override
		public void eSet(EStructuralFeature eStructuralFeature, Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			case ProblemandsolutionModelPackage.WORKAROUND_ACCEPTEDBYCLIENT_FEATURE_ID:
				getTarget().setAcceptedByClient((Boolean) value);
				return;

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eContainer())) {
					getTarget().set(eStructuralFeature.getName(), value);
					return;
				}

			}

			super.eSet(eStructuralFeature, value);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public void eAddTo(EStructuralFeature eStructuralFeature, Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eClass())) {
					@SuppressWarnings("rawtypes")
					List l = (List) getTarget().get(
							eStructuralFeature.getName());
					if (l != null) {
						l.add(value);
					}
					return;
				}

			}

			super.eAddTo(eStructuralFeature, value);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public void eRemoveFrom(EStructuralFeature eStructuralFeature,
				Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eClass())) {
					@SuppressWarnings("rawtypes")
					List l = (List) getTarget().get(
							eStructuralFeature.getName());
					if (l != null) {
						l.remove(value);
					}
					return;
				}

			}

			super.eRemoveFrom(eStructuralFeature, value);
		}
	}

	/**
	 * The adapter/wrapper for the EClass '<em><b>WorkaroundNotes</b></em>'.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static class WorkaroundNotesModelObject<E extends WorkaroundNotes>

	extends AbstractModelObject<E>

	{

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		private ProblemandsolutionModelPackage modelPackage;

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public WorkaroundNotesModelObject(
				ProblemandsolutionModelPackage modelPackage) { // NOSONAR

			this.modelPackage = modelPackage;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public EClass eClass() {
			return modelPackage.getWorkaroundNotesEClass();
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public ModelPackage getModelPackage() {
			return modelPackage;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public Object eGet(EStructuralFeature eStructuralFeature) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			case ProblemandsolutionModelPackage.WORKAROUNDNOTES_ID_FEATURE_ID:
				return getTarget().getId();

			case ProblemandsolutionModelPackage.WORKAROUNDNOTES_WORKAROUNDID_FEATURE_ID:
				return getTarget().getWorkaroundId();

			case ProblemandsolutionModelPackage.WORKAROUNDNOTES_TITLE_FEATURE_ID:
				return getTarget().getTitle();

			case ProblemandsolutionModelPackage.WORKAROUNDNOTES_CONTENT_FEATURE_ID:
				return getTarget().getContent();

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eContainer())) {
					return getTarget().get(eStructuralFeature.getName());
				}

			}

			return super.eGet(eStructuralFeature);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */

		@Override
		public void eSet(EStructuralFeature eStructuralFeature, Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			case ProblemandsolutionModelPackage.WORKAROUNDNOTES_ID_FEATURE_ID:
				getTarget().setId((Long) value);
				return;

			case ProblemandsolutionModelPackage.WORKAROUNDNOTES_WORKAROUNDID_FEATURE_ID:
				getTarget().setWorkaroundId((Long) value);
				return;

			case ProblemandsolutionModelPackage.WORKAROUNDNOTES_TITLE_FEATURE_ID:
				getTarget().setTitle((String) value);
				return;

			case ProblemandsolutionModelPackage.WORKAROUNDNOTES_CONTENT_FEATURE_ID:
				getTarget().setContent((String) value);
				return;

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eContainer())) {
					getTarget().set(eStructuralFeature.getName(), value);
					return;
				}

			}

			super.eSet(eStructuralFeature, value);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public void eAddTo(EStructuralFeature eStructuralFeature, Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eClass())) {
					@SuppressWarnings("rawtypes")
					List l = (List) getTarget().get(
							eStructuralFeature.getName());
					if (l != null) {
						l.add(value);
					}
					return;
				}

			}

			super.eAddTo(eStructuralFeature, value);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public void eRemoveFrom(EStructuralFeature eStructuralFeature,
				Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eClass())) {
					@SuppressWarnings("rawtypes")
					List l = (List) getTarget().get(
							eStructuralFeature.getName());
					if (l != null) {
						l.remove(value);
					}
					return;
				}

			}

			super.eRemoveFrom(eStructuralFeature, value);
		}
	}

	/**
	 * The adapter/wrapper for the EClass '<em><b>Fix</b></em>'.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static class FixModelObject<E extends Fix>

	extends SolutionModelObject<E>

	{

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		private ProblemandsolutionModelPackage modelPackage;

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public FixModelObject(ProblemandsolutionModelPackage modelPackage) { // NOSONAR

			super((ProblemandsolutionModelPackage) modelPackage
					.getModelPackage(ProblemandsolutionModelPackage.class));

			this.modelPackage = modelPackage;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public EClass eClass() {
			return modelPackage.getFixEClass();
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public ModelPackage getModelPackage() {
			return modelPackage;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public Object eGet(EStructuralFeature eStructuralFeature) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			case ProblemandsolutionModelPackage.FIX_COMMITREF_FEATURE_ID:
				return getTarget().getCommitRef();

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eContainer())) {
					return getTarget().get(eStructuralFeature.getName());
				}

			}

			return super.eGet(eStructuralFeature);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */

		@Override
		public void eSet(EStructuralFeature eStructuralFeature, Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			case ProblemandsolutionModelPackage.FIX_COMMITREF_FEATURE_ID:
				getTarget().setCommitRef((String) value);
				return;

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eContainer())) {
					getTarget().set(eStructuralFeature.getName(), value);
					return;
				}

			}

			super.eSet(eStructuralFeature, value);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public void eAddTo(EStructuralFeature eStructuralFeature, Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eClass())) {
					@SuppressWarnings("rawtypes")
					List l = (List) getTarget().get(
							eStructuralFeature.getName());
					if (l != null) {
						l.add(value);
					}
					return;
				}

			}

			super.eAddTo(eStructuralFeature, value);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public void eRemoveFrom(EStructuralFeature eStructuralFeature,
				Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eClass())) {
					@SuppressWarnings("rawtypes")
					List l = (List) getTarget().get(
							eStructuralFeature.getName());
					if (l != null) {
						l.remove(value);
					}
					return;
				}

			}

			super.eRemoveFrom(eStructuralFeature, value);
		}
	}

	/**
	 * The adapter/wrapper for the EClass '<em><b>RefSolutionType</b></em>'.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static class RefSolutionTypeModelObject<E extends RefSolutionType>

	extends AbstractModelObject<E>

	{

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		private ProblemandsolutionModelPackage modelPackage;

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		public RefSolutionTypeModelObject(
				ProblemandsolutionModelPackage modelPackage) { // NOSONAR

			this.modelPackage = modelPackage;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public EClass eClass() {
			return modelPackage.getRefSolutionTypeEClass();
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public ModelPackage getModelPackage() {
			return modelPackage;
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public Object eGet(EStructuralFeature eStructuralFeature) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_SOLUTIONTYPE_FEATURE_ID:
				return getTarget().getSolutionType();

			case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_KEY_FEATURE_ID:
				return getTarget().getKey();

			case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_VALUE_FEATURE_ID:
				return getTarget().getValue();

			case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_DISPLAYVALUE_FEATURE_ID:
				return getTarget().getDisplayValue();

			case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_DISPLAYORDER_FEATURE_ID:
				return getTarget().getDisplayOrder();

			case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_STARTVALIDATIONDATE_FEATURE_ID:
				return getTarget().getStartValidationDate();

			case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_ENDVALIDATIONDATE_FEATURE_ID:
				return getTarget().getEndValidationDate();

			case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_REPLACEDBY_FEATURE_ID:
				return getTarget().getReplacedBy();

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eContainer())) {
					return getTarget().get(eStructuralFeature.getName());
				}

			}

			return super.eGet(eStructuralFeature);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */

		@Override
		public void eSet(EStructuralFeature eStructuralFeature, Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_SOLUTIONTYPE_FEATURE_ID:
				getTarget().setSolutionType((SolutionType) value);
				return;

			case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_KEY_FEATURE_ID:
				getTarget().setKey((String) value);
				return;

			case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_VALUE_FEATURE_ID:
				getTarget().setValue((String) value);
				return;

			case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_DISPLAYVALUE_FEATURE_ID:
				getTarget().setDisplayValue((String) value);
				return;

			case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_DISPLAYORDER_FEATURE_ID:
				getTarget().setDisplayOrder((String) value);
				return;

			case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_STARTVALIDATIONDATE_FEATURE_ID:
				getTarget().setStartValidationDate((Date) value);
				return;

			case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_ENDVALIDATIONDATE_FEATURE_ID:
				getTarget().setEndValidationDate((Date) value);
				return;

			case ProblemandsolutionModelPackage.REFSOLUTIONTYPE_REPLACEDBY_FEATURE_ID:
				getTarget().setReplacedBy((String) value);
				return;

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eContainer())) {
					getTarget().set(eStructuralFeature.getName(), value);
					return;
				}

			}

			super.eSet(eStructuralFeature, value);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public void eAddTo(EStructuralFeature eStructuralFeature, Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eClass())) {
					@SuppressWarnings("rawtypes")
					List l = (List) getTarget().get(
							eStructuralFeature.getName());
					if (l != null) {
						l.add(value);
					}
					return;
				}

			}

			super.eAddTo(eStructuralFeature, value);
		}

		/**
		 * <!-- begin-user-doc --><!-- end-user-doc -->
		 * 
		 * @generated
		 */
		@Override
		public void eRemoveFrom(EStructuralFeature eStructuralFeature,
				Object value) {

			final int featureID = eClass().getFeatureID(eStructuralFeature);
			switch (featureID) {

			default:
				// flex field
				if (eClass().equals(eStructuralFeature.eClass())) {
					@SuppressWarnings("rawtypes")
					List l = (List) getTarget().get(
							eStructuralFeature.getName());
					if (l != null) {
						l.remove(value);
					}
					return;
				}

			}

			super.eRemoveFrom(eStructuralFeature, value);
		}
	}

}
