package com.finantix.problemandsolution.core;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A representation of the literals of the enumeration '
 * <em><b>SolutionType</b></em>'. <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

public enum SolutionType {

	/**
	 * The enum: UNKNOWN. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */

	UNKNOWN(0, "UNKNOWN", "UNKNOWN") {

		/**
		 * Checks the instance.
		 * 
		 * @return always true for this instance
		 * @generated
		 */
		@Override
		public boolean isUNKNOWN() {
			return true;
		}
	}

	,

	/**
	 * The enum: WORKAROUND. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */

	WORKAROUND(1, "WORKAROUND", "WORKAROUND") {

		/**
		 * Checks the instance.
		 * 
		 * @return always true for this instance
		 * @generated
		 */
		@Override
		public boolean isWORKAROUND() {
			return true;
		}
	}

	,

	/**
	 * The enum: FIX. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */

	FIX(2, "FIX", "FIX") {

		/**
		 * Checks the instance.
		 * 
		 * @return always true for this instance
		 * @generated
		 */
		@Override
		public boolean isFIX() {
			return true;
		}
	}

	;

	/**
	 * An array of all the '<em><b>SolutionType</b></em>' enumerators. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static final SolutionType[] VALUES_ARRAY = new SolutionType[] {

	UNKNOWN, WORKAROUND, FIX

	};

	/**
	 * A public read-only list of all the '<em><b>SolutionType</b></em>'
	 * enumerators. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public static final List<SolutionType> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>SolutionType</b></em>' literal with the specified
	 * literal value. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @return the SolutionType literal with the specified literal value
	 * @param literal
	 *            the specified literal value
	 */
	public static SolutionType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SolutionType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>SolutionType</b></em>' literal with the specified
	 * name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @return the SolutionType literal with the specified name
	 * @param name
	 *            the specified name
	 */
	public static SolutionType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SolutionType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>SolutionType</b></em>' literal with the specified
	 * integer value. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @return the SolutionType literal with the specified integer value.
	 * @param value
	 *            the specified integer value.
	 */
	public static SolutionType get(int value) {
		for (SolutionType enumInstance : VALUES_ARRAY) {
			if (enumInstance.getValue() == value) {
				return enumInstance;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	private SolutionType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * Checks the instance. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return false, is overridden by actual enum types.
	 * @generated
	 */
	public boolean isUNKNOWN() {
		return false;
	}

	/**
	 * Checks the instance. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return false, is overridden by actual enum types.
	 * @generated
	 */
	public boolean isWORKAROUND() {
		return false;
	}

	/**
	 * Checks the instance. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return false, is overridden by actual enum types.
	 * @generated
	 */
	public boolean isFIX() {
		return false;
	}

	/**
	 * Returns the integer value . <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the integer value.
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Returns the name. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the literal name.
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the value. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the literal value
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @return the literal value of the enumerator, which is its string
	 *         representation.
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
}
