package com.finantix.problemandsolution.core.service;

import com.finantix.problemandsolution.core.Workaround;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.model.service.IEntityService;
import com.thedigitalstack.model.service.sync.AbstractJPASyncEntity;
import com.thedigitalstack.model.service.sync.ISyncEntity;

/**
 * The Service implementation for the model object '<em><b>Workaround</b></em>'. <br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */

public class WorkaroundSyncEntity extends AbstractJPASyncEntity<Workaround> implements ISyncEntity<Workaround> {

    /**
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected IEntityService<Workaround> getDelegate() {
        return new WorkaroundService();
    }

    /**
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    @NonNull
    protected Class<Workaround> getEntityClass() {
        return Workaround.class;
    }

}
