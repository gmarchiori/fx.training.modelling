package com.finantix.problemandsolution.core.service;

import java.util.List;
import com.finantix.problemandsolution.core.Fix;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.annotation.Nullable;
import com.thedigitalstack.model.query.IQueryParams;
import com.thedigitalstack.model.service.AbstractEntityService;
import com.thedigitalstack.model.service.IEntityService;
import com.thedigitalstack.model.service.IOutMetadata;
import com.thedigitalstack.model.service.ResultSet;
import com.thedigitalstack.model.service.ServiceFactory;

/**
 * 
 * The Service implementation for the model object '<em><b>Fix</b></em>'.<br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */

// <!-- JAX-RS Annotation -->
// @Path(com.finantix.problemandsolution.core.ProblemandsolutionModelPackage.NAME
// + "/" + com.finantix.problemandsolution.core.Fix.CLASS_ID)
public class FixService extends AbstractEntityService<Fix> {

    /**
     * Finds the {@link Fix} with the given id.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     * @param id
     *            {@link Fix} identifier
     * @return a {@link Fix}
     */

    // <!-- JAX-RS Annotation -->
    // @GET
    // @Path("{id}")
    // @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })

    @Override
    @Nullable
    public Fix get( /* @PathParam("id") */@NonNull Object id) {
        return super.get(id);
    }

    /**
     * Gets all {@link Fix} satisfying the given query restriction. If no query
     * parameters are specified, all entities will be returned.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     * @param queryParams
     *            the QueryParams map with the parameters to use in the query
     * @param outMetadata
     *            the IOutMetadata with the response metadata
     * @RSParam l (optional) Max amount of response elements (limit). Example
     *          <i>?l=10</i>
     * @RSParam o (optional) Offset in the data storage of the first response
     *          element. Example <i>?o=10</i>
     * @RSParam by (optional) Ordering criteria of response elements. Example
     *          <i>?by=aFieldName|desc</i>
     * @RSParam filter (optional) Filter criteria of the response. Example
     *          <i>?aFieldName=1|2|3</i>
     * 
     * @return a list of {@link Fix}
     */
    // @ResponseHeaders (
    // {
    // @ResponseHeader( name = "meta-totalCount", description =
    // "Number of existing elements with the query restriction."),
    // @ResponseHeader( name = "meta-partialCount", description =
    // "Number of elements in the response."),
    // @ResponseHeader( name = "meta-offset", description =
    // "Offset in the data storage of the first element of the response."),
    // @ResponseHeader( name = "meta-next", description =
    // "Offset of the next existing element in data storage not included in the response.")
    // }
    // )
    // @ResourceMethodSignature(queryParams={@QueryParam("l"),@QueryParam("o"),@QueryParam("by"),@QueryParam("filter")},
    // output=ProblemandsolutionModelPackage.FixXMLContainer.class)
    @Override
    // @GET
    // @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @NonNull
    public List<Fix> getAll(/* @Context */@NonNull IQueryParams queryParams, /*
                                                                              * @
                                                                              * Context
                                                                              */@Nullable IOutMetadata outMetadata) {
        return super.getAll(queryParams, outMetadata);
    }

    /**
     * Gets a {@link ResultSet} with the selected entity fields.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     * @param queryParams
     *            the QueryParams map with the parameters to use in the query
     * @param outMetadata
     *            the IOutMetadata with the response metadata
     * @RSParam f The {@link Fix} field to insert in the response. Example
     *          <i>?f=aFieldName,...</i>
     * @RSParam l (optional) Max amount of response elements (limit). Example
     *          <i>?l=10</i>
     * @RSParam o (optional) Offset in the data storage of the first response
     *          element. Example <i>?o=10</i>
     * @RSParam by (optional) Ordering criteria of response elements. Example
     *          <i>?by=aFieldName|desc</i>
     * @RSParam filter (optional) Filter criteria of the response. Example
     *          <i>?aFieldName=1|2|3</i>
     * @return a {@link ResultSet}
     */
    // @ResponseHeaders (
    // {
    // @ResponseHeader( name = "meta-totalCount", description =
    // "Number of existing elements with the query restriction."),
    // @ResponseHeader( name = "meta-partialCount", description =
    // "Number of elements in the response."),
    // @ResponseHeader( name = "meta-offset", description =
    // "Offset in the data storage of the first element of the response."),
    // @ResponseHeader( name = "meta-next", description =
    // "Offset of the next existing element in data storage not included in the response.")
    // }
    // )
    // @ResourceMethodSignature(output=ResultSet.class,
    // queryParams={@QueryParam("f"),@QueryParam("l"),@QueryParam("o"),@QueryParam("by"),@QueryParam("filter")})

    @Override
    // @GET
    // @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    // @Path("fields")
    @Nullable
    public ResultSet getResult(/* @Context */@NonNull IQueryParams queryParams, /*
                                                                                 * @
                                                                                 * Context
                                                                                 */@Nullable IOutMetadata outMetadata) {
        return super.getResult(queryParams, outMetadata);
    }

    /**
     * Counts the {@link Fix}s satisfying the query restriction.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     * @param queryParams
     *            the QueryParams map with the parameters to use in the query
     * @RSParam l (optional) Max amount of response elements (limit). Example
     *          <i>?l=10</i>
     * @RSParam o (optional) Offset in the data storage of the first response
     *          element. Example <i>?o=10</i>
     * @RSParam by (optional) Ordering criteria of response elements. Example
     *          <i>?by=aFieldName|desc</i>
     * @RSParam filter (optional) Filter criteria of the response. Example
     *          <i>?aFieldName=1|2|3</i>
     * @return a {@link ResultSet}
     */
    // @ResourceMethodSignature(queryParams={@QueryParam("f"),@QueryParam("l"),@QueryParam("o"),@QueryParam("by"),@QueryParam("filter")},output=Count.class)
    @Override
    // @GET
    // @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    // @Path("count")
    // @ParamConverter(com.thedigitalstack.model.converter.CountConverter.class)
    public int countAll(/* @Context */@NonNull IQueryParams queryParams) {
        return super.countAll(queryParams);
    }

    /**
     * Deletes all {@link Fix} matching the provided parameters.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     * @param queryParams
     *            the QueryParams map with the parameters to use in the query
     * @return the number of deleted entities
     */
    @Override
    // @ResourceMethodSignature(output = Count.class)
    // @DELETE
    // @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    // @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    // @ParamConverter(com.thedigitalstack.model.converter.CountConverter.class)
    public int removeAll(/* @Context */@NonNull IQueryParams queryParams) {
        return super.removeAll(queryParams);
    }

    /**
     * Deletes the {@link Fix} with the given id.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     * @param id
     *            the identifier
     * @return 1 if the entity was found and deleted, 0 otherwise
     */
    @Override
    // @ResourceMethodSignature(output = Count.class)
    // @DELETE
    // @Path("{id}")
    // @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    // @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    // @ParamConverter(CountConverter.class)
    public int removeById(/* @PathParam("id") */@NonNull Object id) {
        return super.removeById(id);
    }

    /**
     * Creates the given {@link Fix} in data storage and returns the saved
     * object. Depending on the storage in use, the returned object may be the
     * same instance as the passed entity or not.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     * @param entity
     *            the {@link Fix} instance
     * @return the entity created in the storage
     */
    // @ResourceMethodSignature(output = IdResult.class)
    @Override
    // @POST
    // @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    // @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    // @ParamConverter(IdConverter.class)
    @NonNull
    public Fix create(@NonNull Fix entity) {
        return super.create(entity);
    }

    /**
     * Creates the given {@link Fix}s in data storage. Depending on the storage
     * in use, the returned objects may be the same instances as the input or
     * not.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     * @param entities
     *            the list of {@link Fix}
     * @return the list of entities created in the storage
     */
    // @ResourceMethodSignature(output = IdContainer.class)
    @Override
    // @POST
    // @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    // @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    // @Path("list")
    // @ParamConverter(IdConverter.class)
    @NonNull
    public List<Fix> create(@NonNull List<Fix> entities) {
        return super.create(entities);
    }

    /**
     * Updates the given {@link Fix} in data storage.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     * @param entity
     *            the {@link Fix} instance
     * @return the entity updated in the storage
     */
    // @ResourceMethodSignature(output=UpdateResult.class)
    @Override
    // @PUT
    // @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    // @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    // @ParamConverter(UpdateConverter.class)
    @NonNull
    public Fix update(@NonNull Fix entity) {
        return super.update(entity);
    }

    /**
     * Updates the given {@link Fix}s in data storage.<br>
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     * @param entities
     *            the list of {@link Fix}
     * @return the entities updated in the storage
     */
    @Override
    // @ResourceMethodSignature(output=UpdateResultContainer.class)
    // @PUT
    // @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    // @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    // @Path("list")
    // @ParamConverter(UpdateConverter.class)
    @NonNull
    public List<Fix> update(@NonNull List<Fix> entities) {
        return super.update(entities);
    }

    /**
     * <!-- begin-user-doc --> <!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    @NonNull
    protected IEntityService<Fix> getDelegate() {
        return ServiceFactory.newDAO(Fix.class);
    }

}
