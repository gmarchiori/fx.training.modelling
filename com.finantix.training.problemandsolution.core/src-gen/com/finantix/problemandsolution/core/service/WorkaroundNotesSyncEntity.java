package com.finantix.problemandsolution.core.service;

import com.finantix.problemandsolution.core.WorkaroundNotes;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.model.service.IEntityService;
import com.thedigitalstack.model.service.sync.AbstractJPASyncEntity;
import com.thedigitalstack.model.service.sync.ISyncEntity;

/**
 * The Service implementation for the model object '
 * <em><b>WorkaroundNotes</b></em>'. <br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */

public class WorkaroundNotesSyncEntity extends AbstractJPASyncEntity<WorkaroundNotes> implements ISyncEntity<WorkaroundNotes> {

    /**
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected IEntityService<WorkaroundNotes> getDelegate() {
        return new WorkaroundNotesService();
    }

    /**
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    @NonNull
    protected Class<WorkaroundNotes> getEntityClass() {
        return WorkaroundNotes.class;
    }

}
