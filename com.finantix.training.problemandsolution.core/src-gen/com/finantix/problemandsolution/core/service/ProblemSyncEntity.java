package com.finantix.problemandsolution.core.service;

import com.finantix.problemandsolution.core.Problem;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.model.service.IEntityService;
import com.thedigitalstack.model.service.sync.AbstractJPASyncEntity;
import com.thedigitalstack.model.service.sync.ISyncEntity;

/**
 * The Service implementation for the model object '<em><b>Problem</b></em>'. <br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */

public class ProblemSyncEntity extends AbstractJPASyncEntity<Problem> implements ISyncEntity<Problem> {

    /**
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected IEntityService<Problem> getDelegate() {
        return new ProblemService();
    }

    /**
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    @NonNull
    protected Class<Problem> getEntityClass() {
        return Problem.class;
    }

}
