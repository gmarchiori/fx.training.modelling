package com.finantix.problemandsolution.core.service;

import com.finantix.problemandsolution.core.Fix;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.model.service.IEntityService;
import com.thedigitalstack.model.service.sync.AbstractJPASyncEntity;
import com.thedigitalstack.model.service.sync.ISyncEntity;

/**
 * The Service implementation for the model object '<em><b>Fix</b></em>'. <br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */

public class FixSyncEntity extends AbstractJPASyncEntity<Fix> implements ISyncEntity<Fix> {

    /**
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected IEntityService<Fix> getDelegate() {
        return new FixService();
    }

    /**
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    @NonNull
    protected Class<Fix> getEntityClass() {
        return Fix.class;
    }

}
