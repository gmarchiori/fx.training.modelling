package com.finantix.problemandsolution.core.service;

import com.finantix.problemandsolution.core.RefSolutionType;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.model.service.IEntityService;
import com.thedigitalstack.model.service.sync.AbstractJPASyncEntity;
import com.thedigitalstack.model.service.sync.ISyncEntity;

/**
 * The Service implementation for the model object '
 * <em><b>RefSolutionType</b></em>'. <br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */

public class RefSolutionTypeSyncEntity extends AbstractJPASyncEntity<RefSolutionType> implements ISyncEntity<RefSolutionType> {

    /**
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    protected IEntityService<RefSolutionType> getDelegate() {
        return new RefSolutionTypeService();
    }

    /**
     * <!-- begin-user-doc --><!-- end-user-doc -->
     * 
     * @generated
     */
    @Override
    @NonNull
    protected Class<RefSolutionType> getEntityClass() {
        return RefSolutionType.class;
    }

}
