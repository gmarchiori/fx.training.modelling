package com.finantix.problemandsolution.core.dao;

import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import com.finantix.problemandsolution.core.ProblemandsolutionModelPackage;
import com.finantix.problemandsolution.core.Workaround;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.annotation.Nullable;
import com.thedigitalstack.model.converter.CountConverter;
import com.thedigitalstack.model.converter.IdContainer;
import com.thedigitalstack.model.converter.IdConverter;
import com.thedigitalstack.model.converter.IdResult;
import com.thedigitalstack.model.converter.ParamConverter;
import com.thedigitalstack.model.converter.UpdateConverter;
import com.thedigitalstack.model.converter.UpdateResult;
import com.thedigitalstack.model.converter.UpdateResultContainer;
import com.thedigitalstack.model.query.IQueryParams;
import com.thedigitalstack.model.service.BaseTenantDao;
import com.thedigitalstack.model.service.Count;
import com.thedigitalstack.model.service.IOutMetadata;
import com.thedigitalstack.model.service.ResultSet;
import com.thedigitalstack.security.Roles;
import com.webcohesion.enunciate.metadata.rs.ResourceMethodSignature;
import com.webcohesion.enunciate.metadata.rs.ResponseHeader;
import com.webcohesion.enunciate.metadata.rs.ResponseHeaders;

/**
 * The Dao implementation for the model object '<em><b>Workaround</b></em>'.<br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */

@RolesAllowed(Roles.ADMIN)
@Path(ProblemandsolutionModelPackage.NAME + "/" + Workaround.CLASS_ID)
public class WorkaroundDao extends BaseTenantDao<Workaround> {

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	@NonNull
	public Class<Workaround> getEntityClass() {
		return Workaround.class;
	}

	// ---- REST services ----

	// ------ Read - GET Begin --------

	/**
	 * Finds the {@link Workaround} with the given id.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 * @param id
	 *            {@link Workaround} identifier
	 * @return the {@link Workaround} identified by the given id, or
	 *         <code>null</code> if it cannot be found
	 */
	@Override
	@GET
	@Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Nullable
	public Workaround get(@PathParam("id") @NonNull Object id) {
		return super.get(id);
	}

	/**
	 * Gets all {@link Workaround} satisfying the given query restriction. If no
	 * query parameters are specified, all entities will be returned.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 * @param queryParams
	 *            the QueryParams map with the parameters to use in the query
	 * @param outMetadata
	 *            the IOutMetadata with the response metadata
	 * @RSParam l (optional) Max amount of response elements (limit). Example
	 *          <i>?l=10</i>
	 * @RSParam o (optional) Offset in the data storage of the first response
	 *          element. Example <i>?o=10</i>
	 * @RSParam by (optional) Ordering criteria of response elements. Example
	 *          <i>?by=aFieldName|desc</i>
	 * @RSParam filter (optional) Filter criteria of the response. Example
	 *          <i>?aFieldName=1|2|3</i>
	 * @return a list of {@link Workaround}
	 */
	@ResponseHeaders({
			@ResponseHeader(name = "meta-totalCount", description = "Number of existing elements with the query restriction."),
			@ResponseHeader(name = "meta-partialCount", description = "Number of elements in the response."),
			@ResponseHeader(name = "meta-offset", description = "Offset in the data storage of the first element of the response."),
			@ResponseHeader(name = "meta-next", description = "Offset of the next existing element in data storage not included in the response.") })
	@ResourceMethodSignature(queryParams = { @QueryParam("l"),
			@QueryParam("o"), @QueryParam("by"), @QueryParam("filter") }, output = ProblemandsolutionModelPackage.WorkaroundXMLContainer.class)
	@Override
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@NonNull
	public List<Workaround> getAll(@Context @NonNull IQueryParams queryParams,
			@Context @Nullable IOutMetadata outMetadata) {
		return super.getAll(queryParams, outMetadata);
	}

	/**
	 * Gets a {@link ResultSet} with the selected entity fields.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 * @param queryParams
	 *            the QueryParams map with the parameters to use in the query
	 * @param outMetadata
	 *            the IOutMetadata with the response metadata
	 * @RSParam f The {@link Workaround} field to insert in the response.
	 *          Example <i>?f=aFieldName,...</i>
	 * @RSParam l (optional) Max amount of response elements (limit). Example
	 *          <i>?l=10</i>
	 * @RSParam o (optional) Offset in the data storage of the first response
	 *          element. Example <i>?o=10</i>
	 * @RSParam by (optional) Ordering criteria of response elements. Example
	 *          <i>?by=aFieldName|desc</i>
	 * @RSParam filter (optional) Filter criteria of the response. Example
	 *          <i>?aFieldName=1|2|3</i>
	 * 
	 * 
	 * @return a {@link ResultSet}
	 */
	@ResponseHeaders({
			@ResponseHeader(name = "meta-totalCount", description = "Number of existing elements with the query restriction."),
			@ResponseHeader(name = "meta-partialCount", description = "Number of elements in the response."),
			@ResponseHeader(name = "meta-offset", description = "Offset in the data storage of the first element of the response."),
			@ResponseHeader(name = "meta-next", description = "Offset of the next existing element in data storage not included in the response.") })
	@ResourceMethodSignature(output = ResultSet.class, queryParams = {
			@QueryParam("f"), @QueryParam("l"), @QueryParam("o"),
			@QueryParam("by"), @QueryParam("filter") })
	@Override
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Path("fields")
	@Nullable
	public ResultSet getResult(@Context @NonNull IQueryParams queryParams,
			@Context @Nullable IOutMetadata outMetadata) {
		return super.getResult(queryParams, outMetadata);
	}

	/**
	 * Counts the {@link Workaround}s satisfying the query restriction.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 * @param queryParams
	 *            the QueryParams map with the parameters to use in the query
	 * @RSParam l (optional) Max amount of response elements (limit). Example
	 *          <i>?l=10</i>
	 * @RSParam o (optional) Offset in the data storage of the first response
	 *          element. Example <i>?o=10</i>
	 * @RSParam by (optional) Ordering criteria of response elements. Example
	 *          <i>?by=aFieldName|desc</i>
	 * @RSParam filter (optional) Filter criteria of the response. Example
	 *          <i>?aFieldName=1|2|3</i>
	 * @return a {@link ResultSet}
	 */
	@ResourceMethodSignature(queryParams = { @QueryParam("f"),
			@QueryParam("l"), @QueryParam("o"), @QueryParam("by"),
			@QueryParam("filter") }, output = Count.class)
	@Override
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Path("count")
	@ParamConverter(CountConverter.class)
	public int countAll(@Context @NonNull IQueryParams queryParams) {
		return super.countAll(queryParams);
	}

	/**
	 * Deletes all {@link Workaround} matching the provided parameters.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 * @param queryParams
	 *            the QueryParams map with the parameters to use in the query
	 * @return the number of deleted entities
	 */
	@ResourceMethodSignature(output = Count.class)
	@Override
	@DELETE
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@ParamConverter(CountConverter.class)
	public int removeAll(@Context @NonNull IQueryParams queryParams) {
		return super.removeAll(queryParams);
	}

	// ------ Read - GET End --------
	// ------ Delete - DELETE Begin --------

	/**
	 * Deletes the {@link Workaround} with the given id.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 * @param id
	 *            the identifier
	 * @return 1 if the entity was found and deleted, 0 otherwise
	 */
	@ResourceMethodSignature(output = Count.class)
	@Override
	@DELETE
	@Path("{id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@ParamConverter(CountConverter.class)
	public int removeById(@PathParam("id") @NonNull Object id) {
		return super.removeById(id);
	}

	// ------ Delete - DELETE End --------
	// ------ Create - POST Begin --------

	/**
	 * Creates the given {@link Workaround} in data storage and returns the
	 * saved object. Depending on the storage in use, the returned object may be
	 * the same instance as the passed entity or not.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 * @param entity
	 *            the {@link Workaround} instance
	 * @return the entity created in the storage
	 */
	@ResourceMethodSignature(output = IdResult.class)
	@Override
	@POST
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@ParamConverter(IdConverter.class)
	@NonNull
	public Workaround create(@NonNull Workaround entity) {
		return super.create(entity);
	}

	/**
	 * Creates the given {@link Workaround}s in data storage. Depending on the
	 * storage in use, the returned objects may be the same instances as the
	 * input or not.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 * @param entities
	 *            the list of {@link Workaround}
	 * @return the list of entities created in the storage
	 */
	@ResourceMethodSignature(output = IdContainer.class)
	@Override
	@POST
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Path("list")
	@ParamConverter(IdConverter.class)
	@NonNull
	public List<Workaround> create(@NonNull List<Workaround> entities) {
		return super.create(entities);
	}

	// ------ Create - POST End --------
	// ------ Update - PUT Begin --------

	/**
	 * Updates the given {@link Workaround} in data storage.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 * @param entity
	 *            the {@link Workaround} instance
	 * @return the entity updated in the storage
	 */
	@ResourceMethodSignature(output = UpdateResult.class)
	@Override
	@PUT
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@ParamConverter(UpdateConverter.class)
	@NonNull
	public Workaround update(@NonNull Workaround entity) {
		return super.update(entity);
	}

	/**
	 * Updates the given {@link Workaround}s in data storage.<br>
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 * @param entities
	 *            the list of {@link Workaround}
	 * @return the entities updated in the storage
	 */
	@Override
	@ResourceMethodSignature(output = UpdateResultContainer.class)
	@PUT
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Path("list")
	@ParamConverter(UpdateConverter.class)
	@NonNull
	public List<Workaround> update(@NonNull List<Workaround> entities) {
		return super.update(entities);
	}

	// ------ Update - PUT End --------

}
