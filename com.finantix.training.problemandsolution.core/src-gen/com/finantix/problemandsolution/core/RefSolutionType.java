package com.finantix.problemandsolution.core;

import java.util.Date;
import java.util.Locale;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.eclipse.persistence.annotations.Index;
import org.eclipse.persistence.annotations.Indexes;
import org.eclipse.persistence.annotations.Multitenant;
import org.eclipse.persistence.annotations.TenantDiscriminatorColumn;
import org.eclipse.persistence.annotations.TenantDiscriminatorColumns;
import org.eclipse.persistence.annotations.VirtualAccessMethods;
import org.eclipse.persistence.oxm.annotations.XmlVirtualAccessMethods;
import com.finantix.training.shared.core.RefData;
import com.thedigitalstack.annotation.NonNull;
import com.thedigitalstack.annotation.Nullable;
import com.thedigitalstack.model.l10n.LocalizedEntity;
import com.thedigitalstack.model.l10n.adapter.L10NFieldXmlAdapter;

/**
 * A representation of the model object '<em><b>RefSolutionType</b></em>'.<br>
 * <!-- begin-user-doc --><!-- end-user-doc -->
 * 
 * @generated
 */
@Entity(name = "problemandsolution_RefSolutionType")
@Multitenant()
@TenantDiscriminatorColumns({ @TenantDiscriminatorColumn(name = "tenant_id", primaryKey = true) })
@Indexes({
		@Index(columnNames = { "lang", "field" }, name = "problemandsolution_RefSolutionType_l10n", table = "problemandsolution_RefSolutionType_l10nvalues"),
		@Index(columnNames = { "lang" }, name = "problemandsolution_RefSolutionType_l10n_lang", table = "problemandsolution_RefSolutionType_l10nvalues"),
		@Index(columnNames = { "field" }, name = "problemandsolution_RefSolutionType_l10n_field", table = "problemandsolution_RefSolutionType_l10nvalues") })
@XmlVirtualAccessMethods()
@XmlRootElement(namespace = ProblemandsolutionModelPackage.NS_URI)
@VirtualAccessMethods()
public class RefSolutionType extends LocalizedEntity implements RefData

{
	/**
	 * @generated
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Basic()
	@Enumerated(EnumType.STRING)
	private SolutionType solutionType = null;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Id()
	@Column(length = 64, nullable = false)
	private String key = null;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Basic()
	private String value = null;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Basic()
	@Temporal(TemporalType.DATE)
	private Date startValidationDate = null;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Basic()
	@Temporal(TemporalType.DATE)
	private Date endValidationDate = null;

	/**
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Basic()
	private String replacedBy = null;

	/**
	 * Identifier of the class.
	 * 
	 * @generated
	 */
	public static final String CLASS_ID = "refSolutionTypes";

	/**
	 * The feature id for the '{@link RefSolutionType <em>displayValue</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static final String LOCALIZED_FIELD_DISPLAYVALUE = "displayValue";

	/**
	 * Returns the value of '<em><b>displayValue</b></em>' feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>displayValue</b></em>' feature
	 * @generated
	 */
	@XmlJavaTypeAdapter(L10NFieldXmlAdapter.class)
	@Nullable
	public String getDisplayValue() {
		return getField(LOCALIZED_FIELD_DISPLAYVALUE);
	}

	/**
	 * Returns the value of '<em><b>displayValue</b></em>' feature for a given
	 * locale. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>displayValue</b></em>' feature
	 * @generated
	 */
	@Nullable
	public String getDisplayValue(@NonNull Locale locale) {
		return getField(locale, LOCALIZED_FIELD_DISPLAYVALUE);
	}

	/**
	 * Sets the '{@link RefSolutionType#getDisplayValue() <em>displayValue</em>}
	 * ' feature.
	 * 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param newDisplayValue
	 *            the new value of the '
	 *            {@link RefSolutionType#getDisplayValue() displayValue}'
	 *            feature.
	 * @generated
	 */
	public void setDisplayValue(String newDisplayValue) {
		setField(LOCALIZED_FIELD_DISPLAYVALUE, newDisplayValue);
	}

	/**
	 * Sets the '{@link RefSolutionType#getDisplayValue() <em>displayValue</em>}
	 * ' feature for a given locale.
	 * 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param newDisplayValue
	 *            the new value of the '
	 *            {@link RefSolutionType#getDisplayValue() displayValue}'
	 *            feature.
	 * @generated
	 */
	public void setDisplayValue(@NonNull Locale locale, String newDisplayValue) {
		setField(locale, LOCALIZED_FIELD_DISPLAYVALUE, newDisplayValue);
	}

	/**
	 * The feature id for the '{@link RefSolutionType <em>displayOrder</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	private static final String LOCALIZED_FIELD_DISPLAYORDER = "displayOrder";

	/**
	 * Returns the value of '<em><b>displayOrder</b></em>' feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>displayOrder</b></em>' feature
	 * @generated
	 */
	@XmlJavaTypeAdapter(L10NFieldXmlAdapter.class)
	@Nullable
	public String getDisplayOrder() {
		return getField(LOCALIZED_FIELD_DISPLAYORDER);
	}

	/**
	 * Returns the value of '<em><b>displayOrder</b></em>' feature for a given
	 * locale. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>displayOrder</b></em>' feature
	 * @generated
	 */
	@Nullable
	public String getDisplayOrder(@NonNull Locale locale) {
		return getField(locale, LOCALIZED_FIELD_DISPLAYORDER);
	}

	/**
	 * Sets the '{@link RefSolutionType#getDisplayOrder() <em>displayOrder</em>}
	 * ' feature.
	 * 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param newDisplayOrder
	 *            the new value of the '
	 *            {@link RefSolutionType#getDisplayOrder() displayOrder}'
	 *            feature.
	 * @generated
	 */
	public void setDisplayOrder(String newDisplayOrder) {
		setField(LOCALIZED_FIELD_DISPLAYORDER, newDisplayOrder);
	}

	/**
	 * Sets the '{@link RefSolutionType#getDisplayOrder() <em>displayOrder</em>}
	 * ' feature for a given locale.
	 * 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param newDisplayOrder
	 *            the new value of the '
	 *            {@link RefSolutionType#getDisplayOrder() displayOrder}'
	 *            feature.
	 * @generated
	 */
	public void setDisplayOrder(@NonNull Locale locale, String newDisplayOrder) {
		setField(locale, LOCALIZED_FIELD_DISPLAYORDER, newDisplayOrder);
	}

	/**
	 * Returns the value of '<em><b>solutionType</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>solutionType</b></em>' feature
	 * @generated
	 */
	public SolutionType getSolutionType() {
		return solutionType;
	}

	/**
	 * Sets the '{@link RefSolutionType#getSolutionType() <em>solutionType</em>}
	 * ' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newSolutionType
	 *            the new value of the '
	 *            {@link RefSolutionType#getSolutionType() solutionType}'
	 *            feature.
	 * @generated
	 */
	public void setSolutionType(SolutionType newSolutionType) {
		solutionType = newSolutionType;

	}

	/**
	 * Returns the value of '<em><b>key</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>key</b></em>' feature
	 * @generated
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Sets the '{@link RefSolutionType#getKey() <em>key</em>}' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newKey
	 *            the new value of the '{@link RefSolutionType#getKey() key}'
	 *            feature.
	 * @generated
	 */
	public void setKey(String newKey) {
		key = newKey;

	}

	/**
	 * Returns the value of '<em><b>value</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>value</b></em>' feature
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the '{@link RefSolutionType#getValue() <em>value</em>}' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newValue
	 *            the new value of the '{@link RefSolutionType#getValue() value}
	 *            ' feature.
	 * @generated
	 */
	public void setValue(String newValue) {
		value = newValue;

	}

	/**
	 * Returns the value of '<em><b>startValidationDate</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>startValidationDate</b></em>' feature
	 * @generated
	 */
	public Date getStartValidationDate() {
		return startValidationDate;
	}

	/**
	 * Sets the '{@link RefSolutionType#getStartValidationDate()
	 * <em>startValidationDate</em>}' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newStartValidationDate
	 *            the new value of the '
	 *            {@link RefSolutionType#getStartValidationDate()
	 *            startValidationDate}' feature.
	 * @generated
	 */
	public void setStartValidationDate(Date newStartValidationDate) {
		startValidationDate = newStartValidationDate;

	}

	/**
	 * Returns the value of '<em><b>endValidationDate</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>endValidationDate</b></em>' feature
	 * @generated
	 */
	public Date getEndValidationDate() {
		return endValidationDate;
	}

	/**
	 * Sets the '{@link RefSolutionType#getEndValidationDate()
	 * <em>endValidationDate</em>}' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newEndValidationDate
	 *            the new value of the '
	 *            {@link RefSolutionType#getEndValidationDate()
	 *            endValidationDate}' feature.
	 * @generated
	 */
	public void setEndValidationDate(Date newEndValidationDate) {
		endValidationDate = newEndValidationDate;

	}

	/**
	 * Returns the value of '<em><b>replacedBy</b></em>' feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @return the value of '<em><b>replacedBy</b></em>' feature
	 * @generated
	 */
	public String getReplacedBy() {
		return replacedBy;
	}

	/**
	 * Sets the '{@link RefSolutionType#getReplacedBy() <em>replacedBy</em>}'
	 * feature.
	 * 
	 * <!-- begin-user-doc --><!-- end-user-doc -->
	 * 
	 * @param newReplacedBy
	 *            the new value of the '{@link RefSolutionType#getReplacedBy()
	 *            replacedBy}' feature.
	 * @generated
	 */
	public void setReplacedBy(String newReplacedBy) {
		replacedBy = newReplacedBy;

	}

}
