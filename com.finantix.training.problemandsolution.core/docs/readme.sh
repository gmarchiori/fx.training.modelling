## ADMIN LEVEL REST API

# admin - create
curl -XPOST http://127.0.0.1:8080/rest/admin/problemandsolution/solutions.json -u fxposImport_HK:12345678 -d '{"type":"solution","code":"SOL001","description":"Solution 001","id":-1}'

# count 
curl -XGET http://127.0.0.1:8080/rest/admin/problemandsolution/solutions/count.json -u fxposImport_HK:12345678 -s | jq .
# { "value": 1 }

# admin - list all (paginated)
curl -XGET http://127.0.0.1:8080/rest/admin/problemandsolution/solutions.json -u fxposImport_HK:12345678 -s | jq .

# admin - list all (unlimited)
curl -XGET http://127.0.0.1:8080/rest/admin/problemandsolution/solutions.json?l=-1 -u fxposImport_HK:12345678 -s | jq .

# list all (filtered)
curl -XGET http://127.0.0.1:8080/rest/admin/problemandsolution/solutions.json?code=SOL001 -u fxposImport_HK:12345678 -s | jq . 



## USER LEVEL REST API
# list all
curl -XGET http://127.0.0.1:8080/rest/problemandsolution/solutions.json -u FA000001:12345678 -s | jq . 

# create 
## no id specified
curl -XPOST http://127.0.0.1:8080/rest/problemandsolution/solutions.json -u FA000001:12345678 -d '{"type":"solution","code":"SOL003","description":"Solution 003"}'

## create/force id
curl -XPOST http://127.0.0.1:8080/rest/problemandsolution/solutions.json -u FA000001:12345678 -d '{"type":"solution","code":"SOL00x","description":"Solution 00x","id":-1}'

# update
curl -XPUT http://127.0.0.1:8080/rest/problemandsolution/solutions.json -u FA000001:12345678 -d '{"type":"solution","code":"SOL002","description":"Solution 002 - updated","id":52,"updateVersion":2}'
curl -XPUT http://127.0.0.1:8080/rest/problemandsolution/solutions.json -u FA000001:12345678 -d '{"type":"solution","code":"SOL002","description":"Solution 002 - updated","id":52,"updateVersion":-1}'

# delete
curl -XDELETE http://127.0.0.1:8080/rest/problemandsolution/solutions/51.json -u FA000001:12345678
# {"value":1}

# delete all
curl -XDELETE http://127.0.0.1:8080/rest/problemandsolution/solutions.json -u FA000001:12345678
# {"value":3}


# WS call - REST entry point
# admin - create solution - no problem
curl -XPOST http://127.0.0.1:8080/rest/admin/problemandsolution/solutions.json -u fxposImport_HK:12345678 -d '{"type":"solution","code":"SOL001","description":"Solution 001","id":-1}'
# check created solution
curl -XGET http://127.0.0.1:8080/rest/admin/problemandsolution/solutions/-1.json -u FA000001:12345678 -s | jq .

curl -XPOST http://127.0.0.1:8080/rest/admin/problemandsolution/problems.json -u fxposImport_HK:12345678 -d '{"code":"PRO001","description":"Problem 001","id":-1}'
curl -XPOST http://127.0.0.1:8080/rest/admin/problemandsolution/problems.json -u fxposImport_HK:12345678 -d '{"code":"PRO002","description":"Problem 002","id":-2}'
curl -XPOST http://127.0.0.1:8080/rest/admin/problemandsolution/problems.json -u fxposImport_HK:12345678 -d '{"code":"PRO003","description":"Problem 003","id":-3}'

curl -XGET http://127.0.0.1:8080/rest/problemandsolution/solutions/getSolutionDetail/SOL001.json -u FA000001:12345678 -s | jq .


