package com.finantix.training.problemandsolution.test;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;

import com.finantix.problemandsolution.core.bean.FixInfoByReleaseVersionBean;
import com.finantix.problemandsolution.core.bean.WorkaroundInfoByReleaseVersionBean;
import com.finantix.problemandsolution.core.service.custom.CustomFixService;
import com.finantix.problemandsolution.core.service.custom.CustomWorkaroundService;
import com.thedigitalstack.authentication.spi.IUserPrincipal;
import com.thedigitalstack.authentication.spi.UserPrincipal;
import com.thedigitalstack.core.Platform;
import com.thedigitalstack.junit.framework.RunAsTestCase;
import com.thedigitalstack.junit.framework.annotation.RunAsPrincipal;
import com.thedigitalstack.model.persistence.RunnableInTransactionAdapter;
import com.thedigitalstack.model.service.ExecutionContext;
import com.thedigitalstack.model.service.ServiceFactory;
import com.thedigitalstack.util.Assert;

public class AdvancedModelMappingTest extends RunAsTestCase {

    public static final String TENANT_ID = "JP";
    public static final String USERNAME = "PA000001";
    public static final String PASSWORD = "12345678";
    public static final IUserPrincipal TDS_PRINCIPAL = new UserPrincipal(USERNAME, TENANT_ID);

    private static Logger logger = null;
    
    @Override
    @RunAsPrincipal(tenantId = TENANT_ID, userName = USERNAME)
    public void setUp() {
        logger = Platform.getLogger(this.getClass());
        
        ExecutionContext.runLocal(new RunnableInTransactionAdapter() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                ProblemAndSolutionFiller filler = new ProblemAndSolutionFiller();
                try {
                    filler.fillRefSolutionType();
                } catch (Exception e) {
                    logger.error("Setup test issue", e);
                }
                try {
                    filler.fillProblemsAndSolutions();
                } catch (Exception e) {
                    logger.error("Setup test issue", e);
                }
            }
        });
        
    }

    @Override
    public void tearDown() throws IOException {
        // TODO
    }

    @RunAsPrincipal(userName = USERNAME, tenantId = TENANT_ID)
    public void testWorkaroundCreation() {
        CustomWorkaroundService svc = ServiceFactory.newService(CustomWorkaroundService.class);
        List<WorkaroundInfoByReleaseVersionBean> workaroundByRelaseVersion = svc.getWorkaroundByRelaseVersion("05.01.30");
        Assert.isTrue(workaroundByRelaseVersion.size() > 0);
    }

    
    @RunAsPrincipal(userName = USERNAME, tenantId = TENANT_ID)
    public void testFixCreation() {
        CustomFixService svc = ServiceFactory.newService(CustomFixService.class);
        List<FixInfoByReleaseVersionBean> fixByRelaseVersion = svc.getFixByRelaseVersion("05.01.29");
        Assert.isTrue(fixByRelaseVersion.size() > 0);
    }
}