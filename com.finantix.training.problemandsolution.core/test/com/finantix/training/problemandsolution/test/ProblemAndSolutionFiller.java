package com.finantix.training.problemandsolution.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import com.finantix.problemandsolution.core.Fix;
import com.finantix.problemandsolution.core.Problem;
import com.finantix.problemandsolution.core.ProblemandsolutionModelPackage;
import com.finantix.problemandsolution.core.RefSolutionType;
import com.finantix.problemandsolution.core.Solution;
import com.finantix.problemandsolution.core.SolutionType;
import com.finantix.problemandsolution.core.Workaround;
import com.finantix.problemandsolution.core.WorkaroundNotes;
import com.finantix.problemandsolution.core.dao.FixDao;
import com.finantix.problemandsolution.core.dao.ProblemDao;
import com.finantix.problemandsolution.core.dao.RefSolutionTypeDao;
import com.finantix.problemandsolution.core.dao.WorkaroundDao;
import com.finantix.problemandsolution.core.dao.WorkaroundNotesDao;
import com.thedigitalstack.model.metadata.TenantModelPackage;
import com.thedigitalstack.model.query.IPredicate;
import com.thedigitalstack.model.query.IQueryParams;
import com.thedigitalstack.model.query.builder.QueryParamsBuilders;
import com.thedigitalstack.model.service.IDao;
import com.thedigitalstack.model.service.ServiceFactory;

public class ProblemAndSolutionFiller {
    public void fillProblemsAndSolutions() throws ParseException{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        ProblemandsolutionModelPackage mp = TenantModelPackage.findModelPackage(Solution.class);
        
        IDao<Workaround> workaroundDao = ServiceFactory.newDAO(WorkaroundDao.class);
        IDao<Problem> problemDao = ServiceFactory.newDAO(ProblemDao.class);
        String code = "P001";
        IPredicate problemByCode = QueryParamsBuilders.predicateBuilder().equal(mp.getProblem_Code().getName(), code);
        
        IQueryParams buildQueryParams = QueryParamsBuilders.andExpressionBuilder(problemByCode).buildQueryParams();
        
        List<Problem> problemList = problemDao.getAll(buildQueryParams, null);
        
        if(problemList.size()==0){
            Problem problem = new Problem();
            problem.setCode(code);
            problem.setDescription("EclipseLink doesn't manage relation in an abstract superclass");
            problem.setLastUpdated(new Date());
            
            problem = problemDao.create(problem);
            
            Workaround workaround1 = new Workaround();
            List<Problem> problemRel = new ArrayList<Problem>();
            problemRel.add(problem);
            workaround1.setProblemRel(problemRel);
            workaround1.setAcceptedByClient(true);
            workaround1.setCode("W001");
            workaround1.setDeliveryDate(sdf.parse("2016-09-25"));
            workaround1.setDescription("Move the relation in subclasses");
            workaround1.setReleasedIn("05.01.30");
            
            SolutionType workAround = SolutionType.WORKAROUND;
            
            IPredicate solTypeByType = QueryParamsBuilders.predicateBuilder().equal(mp.getRefSolutionType_SolutionType().getName(), workAround);
            
            buildQueryParams = QueryParamsBuilders.andExpressionBuilder(solTypeByType).buildQueryParams();
            
            IDao<RefSolutionType> solTypeDao = ServiceFactory.newDAO(RefSolutionTypeDao.class);
            List<RefSolutionType> allSolTypes = solTypeDao.getAll(buildQueryParams, null);
            if(allSolTypes.size()==1){
                workaround1.setSolutionType(allSolTypes.get(0).getKey());
            }
            
            workaroundDao.create(workaround1);
            
            Workaround workaround2 = new Workaround();
            problemRel = new ArrayList<Problem>();
            problemRel.add(problem);
            workaround2.setProblemRel(problemRel);
            workaround2.setAcceptedByClient(true);
            workaround2.setCode("W002");
            workaround2.setDeliveryDate(sdf.parse("2016-09-28"));
            workaround2.setDescription("Remove ordering information from the relation");
            workaround2.setReleasedIn("05.01.30");
            
            workaround2.setSolutionType(workaround1.getSolutionType());
            
            workaround2 = workaroundDao.create(workaround2);
            IDao<WorkaroundNotes> wnDao = ServiceFactory.newDAO(WorkaroundNotesDao.class);
            
            WorkaroundNotes workaroundNotes = new WorkaroundNotes();
            workaroundNotes.setWorkaroundId(workaround2.getId());
            
            workaroundNotes.setTitle(Locale.ENGLISH, "Cons");
            workaroundNotes.setContent(Locale.ENGLISH, "Sorting feature no more available");
            
            workaroundNotes.setTitle(Locale.JAPANESE, "Japanese: Cons");
            workaroundNotes.setContent(Locale.JAPANESE, "Japanese: Sorting feature no more available");
            
            
            wnDao.create(workaroundNotes);
        }
        
        code = "P002";
        problemByCode = QueryParamsBuilders.predicateBuilder().equal(mp.getProblem_Code().getName(), code);
        
        buildQueryParams = QueryParamsBuilders.andExpressionBuilder(problemByCode).buildQueryParams();
        
        problemList = problemDao.getAll(buildQueryParams, null);
        
        if(problemList.size()==0){
            Problem problem = new Problem();
            problem.setCode(code);
            problem.setDescription("Nashteck team requires more training on model designing");
            problem.setLastUpdated(new Date());
            
            problem = problemDao.create(problem);
            
            Problem problem2 = new Problem();
            problem2.setCode("P003");
            problem2.setDescription("Nashteck team has to implement the model for a new functionality");
            problem2.setLastUpdated(new Date());
            
            problem2 = problemDao.create(problem2);
            
            List<Problem> problemRel = new ArrayList<Problem>();
            problemRel.add(problem);
            problemRel.add(problem2);
            
            IDao<Fix> fixDao = ServiceFactory.newDAO(FixDao.class);
            
            Fix fix = new Fix();
            fix.setCode("F001");
            fix.setCommitRef("F001-001-001-001");
            fix.setDeliveryDate(sdf.parse("2016-09-10"));
            fix.setDescription("Training provided to the team");
            fix.setReleasedIn("05.01.29");
            fix.setProblemRel(problemRel);
            
            SolutionType fixType = SolutionType.FIX;
            IPredicate solTypeByType = QueryParamsBuilders.predicateBuilder().equal(mp.getRefSolutionType_SolutionType().getName(), fixType);
            
            buildQueryParams = QueryParamsBuilders.andExpressionBuilder(solTypeByType).buildQueryParams();
            
            IDao<RefSolutionType> solTypeDao = ServiceFactory.newDAO(RefSolutionTypeDao.class);
            List<RefSolutionType> allSolTypes = solTypeDao.getAll(buildQueryParams, null);
            if(allSolTypes.size()==1){
                fix.setSolutionType(allSolTypes.get(0).getKey());
            }
            
            fixDao.create(fix);
        }
    }
    public void fillRefSolutionType(){
        IDao<RefSolutionType> dao = ServiceFactory.newDAO(RefSolutionTypeDao.class);
        String workAroundKey = "WORKAROUND";
        RefSolutionType refSolutionType = dao.get(workAroundKey);
        if(refSolutionType==null){
            refSolutionType = new RefSolutionType();
            refSolutionType.setKey(workAroundKey);
            refSolutionType.setDisplayOrder("0");
            refSolutionType.setDisplayValue("Workaround");
            refSolutionType.setSolutionType(SolutionType.WORKAROUND);
            dao.create(refSolutionType);
        }
        
        workAroundKey = "FIX";
        refSolutionType = dao.get(workAroundKey);
        if(refSolutionType==null){
            refSolutionType = new RefSolutionType();
            refSolutionType.setKey(workAroundKey);
            refSolutionType.setDisplayOrder("1");
            refSolutionType.setDisplayValue("Fix");
            refSolutionType.setSolutionType(SolutionType.FIX);
            dao.create(refSolutionType);
        }
    }
}
