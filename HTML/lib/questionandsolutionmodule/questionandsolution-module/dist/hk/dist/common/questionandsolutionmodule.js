(function() {
	'use strict';
             
	angular
		.module('com.finantix.questionandsolution', []);
})(); 
(function() {
	'use strict';

	angular
		.module('com.finantix.questionandsolution')
	    .factory('questionandsolutionResourceService', ['resourcePrefixIndex', function(resourcePrefixIndex) {
	    	return resourcePrefixIndex.createModuleResourceService('com.finantix.questionandsolution');
	    }]);
})(); 
