'use strict';

var gulp = require('gulp');
var requireDir = require('require-dir');
var tasks = requireDir('../../../builder.tools/gulp/tasks');

var configRegional = require('./regional/gulp-config-regional.js');
var configTenant = require('./tenant/gulp-config-tenant.js');

var config = {
    moduleNameDir: 'application-module',
    regional: configRegional,
    tenant: configTenant
};

for (var taskName in tasks) {
	if(tasks.hasOwnProperty(taskName)) {
		gulp.task(taskName, tasks[taskName](config));
	}
}